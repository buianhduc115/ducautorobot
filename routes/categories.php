<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategorieController;

Route::middleware(['checkRole'])->group(function () {
    Route::controller(CategorieController::class)->group(function () {
        Route::prefix('categories')->group(function () {
            Route::as('categories.')->group(function () {
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware(['checkPermission:categories.destroy']);
                Route::get('/getListCategory', 'getListCategory')->name('ListCategory');
                Route::get('/', 'index')->name('index')->middleware(['checkPermission:categories.index']);
                Route::get('/create', 'create')->name('create')->middleware(['checkPermission:categories.create']);
                Route::post('/', 'store')->name('store')->middleware(['checkPermission:categories.store']);
                Route::get('/{id}', 'show')->name('show')->middleware(['checkPermission:categories.show']);
                Route::get('/{id}/edit', 'edit')->name('edit')->middleware(['checkPermission:categories.edit']);
                Route::put('/{id}', 'update')->name('update')->middleware(['checkPermission:categories.update']);
            });
        });
    });
});
