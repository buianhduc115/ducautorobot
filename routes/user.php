<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

Route::middleware(['checkRole'])->group(function () {
   Route::controller(UserController::class)->group(function () {
       Route::prefix('users')->group(function () {
           Route::as('users.')->group(function () {
               Route::get('/', 'index')->name('index')->middleware('checkPermission:users.index');
               Route::get('/create', 'create')->name('create')->middleware('checkPermission:users.create');
               Route::post('/', 'store')->name('store')->middleware('checkPermission:users.store');
               Route::get('/show/{id}', 'show')->name('show')->middleware('checkPermission:users.show');
               Route::get('/{id}/edit', 'edit')->name('edit')->middleware('checkPermission:users.edit');
               Route::put('/{id}', 'update')->name('update')->middleware('checkPermission:users.update');
               Route::delete('/{id}', 'destroy')->name('destroy')->middleware('checkPermission:users.destroy');
               Route::get('/information/{id}', 'information')->name('information')->middleware('checkPermission:users.information');
           });
       });
   });
});


