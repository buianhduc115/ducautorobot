<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::middleware(['checkRole'])->group(function () {
    Route::controller(ProductController::class)->group(function () {
        Route::prefix('products')->group(function () {
            Route::as('products.')->group(function () {
                Route::get('/', 'index')->name('index')->middleware('checkPermission:products.index');
                Route::post('/list', 'list')->name('list');
                Route::get('/create', 'create')->name('create')->middleware('checkPermission:products.create');
                Route::post('/store', 'store')->name('store')->middleware('checkPermission:products.store');
                Route::get('/{id}', 'show')->name('show')->middleware('checkPermission:products.show');
                Route::get('/{id}/edit', 'edit')->name('edit')->middleware('checkPermission:products.edit');
                Route::put('/{id}', 'update')->name('update')->middleware('checkPermission:products.update');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('checkPermission:products.destroy');
            });
        });
    });
});
