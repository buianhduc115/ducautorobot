<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\VNPayController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/search', [HomeController::class, 'search'])->name('products.search');
Route::get('/categories-guest/{id}', [CategorieController::class, 'list'])->name('category.product.list');
Route::get('/sign-in', [SessionsController::class, 'create'])->name('login');
Route::post('/sign-in', [SessionsController::class, 'store']);
Route::get('/sign-up', [RegisterController::class, 'create'])->name('signup');
Route::post('/sign-up', [RegisterController::class, 'store'])->name('registration_process');
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auths')->name('dashboard');
Route::get('/logout', [SessionsController::class, 'destroy'])->name('logout');
Route::get('/auth/google', [UserController::class, 'redirectToGoogle']);
Route::get('/auth/google/callback',  [UserController::class, 'handleGoogleCallback']);
Route::get('/products/{id}/detail', [CategorieController::class, 'showDetailProduct'])->name('products.showDetail');

Route::middleware(['checkRole'])->group(function () {
    Route::get('carts',[CartController::class, 'index'])->name('client.carts.index')->middleware('checkPermission:coupons.index');
    Route::post('add-to-cart', [CartController::class, 'store'])->name('client.carts.add')->middleware('checkPermission:coupons.index');
    Route::post('update-quantity-product-in-cart/{cart_product_id}', [CartController::class, 'updateQuantityProduct'])->name('client.carts.update_product_quantity')->middleware('checkPermission:coupons.index');
    Route::post('remove-product-in-cart/{cart_product_id}', [CartController::class, 'removeProductInCart'])->name('client.carts.remove_product')->middleware('checkPermission:coupons.index');
    Route::post('apply-coupon', [CartController::class, 'applyCoupon'])->name('client.carts.apply_coupon');
    Route::get('checkout', [CartController::class, 'checkout'])->name('client.checkout.index')->middleware('user.can_checkout_cart');
    Route::post('process-checkout', [CartController::class, 'processCheckout'])->name('client.checkout.proccess')->middleware('user.can_checkout_cart');
    Route::get('list-orders', [OrderController::class, 'index'])->name('client.orders.index');
    Route::post('orders/cancel/{id}', [OrderController::class, 'cancel'])->name('client.orders.cancel');
    Route::get('orders', [OrderController::class, 'indexAdmin'])->name('admin.orders.index');
    Route::post('update-status/{id}', [OrderController::class, 'updateStatus'])->name('admin.orders.update_status');
    Route::post('vnpay', [VNPayController::class, 'createPayment'])->name('vnpay.create');
    Route::get('vnpay/return', [VNPayController::class, 'return'])->name('vnpay.return');
});

