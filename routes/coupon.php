<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CounponController;

Route::middleware(['checkRole'])->group(function () {
    Route::controller(CounponController::class)->group(function () {
        Route::prefix('coupons')->group(function () {
            Route::as('coupons.')->group(function () {
                Route::get('/', 'index')->name('index')->middleware('checkPermission:coupons.index');
                Route::get('/create', 'create')->name('create')->middleware('checkPermission:coupons.create');
                Route::post('/', 'store')->name('store')->middleware('checkPermission:coupons.store');
                Route::get('/show/{id}', 'show')->name('show')->middleware('checkPermission:coupons.show');
                Route::get('/{id}/edit', 'edit')->name('edit')->middleware('checkPermission:coupons.edit');
                Route::put('/{id}', 'update')->name('update')->middleware('checkPermission:coupons.update');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('checkPermission:coupons.destroy');
            });
        });
    });
});


