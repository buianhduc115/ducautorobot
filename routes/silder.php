<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SilderController;

Route::middleware(['checkRole'])->group(function () {
    Route::controller(SilderController::class)->group(function () {
        Route::prefix('silders')->group(function () {
            Route::as('silders.')->group(function () {
                Route::get('/', 'index')->name('index')->middleware('checkPermission:silders.index');
                Route::get('/create', 'create')->name('create')->middleware('checkPermission:silders.create');
                Route::post('/', 'store')->name('store')->middleware('checkPermission:silders.store');
                Route::get('/show/{id}', 'show')->name('show')->middleware('checkPermission:silders.show');
                Route::get('/{id}/edit', 'edit')->name('edit')->middleware('checkPermission:silders.edit');
                Route::put('/{id}', 'update')->name('update')->middleware('checkPermission:silders.update');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('checkPermission:silders.destroy');
            });
        });
    });
});


