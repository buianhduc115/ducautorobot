<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;

Route::middleware(['checkRole'])->group(function () {
    Route::controller(RoleController::class)->group(function () {
        Route::prefix('roles')->group(function () {
            Route::as('roles.')->group(function () {
                Route::get('/', 'index')->name('index')->middleware('checkPermission:roles.index');
                Route::get('/create', 'create')->name('create')->middleware('checkPermission:roles.create');
                Route::post('/store', 'store')->name('store')->middleware('checkPermission:roles.store');
                Route::get('/{id}', 'show')->name('show')->middleware('checkPermission:roles.show');
                Route::get('/{id}/edit', 'edit')->name('edit')->middleware('checkPermission:roles.edit');
                Route::put('/{id}', 'update')->name('update')->middleware('checkPermission:roles.update');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('checkPermission:roles.destroy');
            });
        });
    });
});
