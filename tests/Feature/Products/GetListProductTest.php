<?php

namespace Tests\Feature\Products;

use App\Repositories\UserRepository;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListProductTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test  */
    public function authenticated_user_can_get_list_product()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_product()
    {
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }
    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _getRouteIndex()
    {
        return route('products.index');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
