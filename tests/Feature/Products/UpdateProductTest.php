<?php

namespace Tests\Feature\Products;

use App\Repositories\UserRepository;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;

class UpdateProductTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function authenticated_user_can_update_users_if_data_pass_validate()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = $this->_makeProductDataFake();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $product = Product::find($createFakeProDuctData->id);
        $this->assertEquals($makeFakeProDuctDataUpdate['name'], $product->name);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_name_is_null()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = Product::factory()->make([
            'name' => null
        ])->toArray();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['name' => 'The name field is required.']);
        $this->assertEquals($createFakeProDuctData->fresh()->name, $createFakeProDuctData->name);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_price_is_not_invalid()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = Product::factory()->make([
            'price' => 'tien'
        ])->toArray();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['price' => 'The price field must be an integer.']);
        $this->assertEquals($createFakeProDuctData->fresh()->price, $createFakeProDuctData->price);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_stock_is_null()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = Product::factory()->make([
            'stock' => null
        ])->toArray();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['stock' => 'The stock field is required.']);
        $this->assertEquals($createFakeProDuctData->fresh()->stock, $createFakeProDuctData->stock);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_image_is_not_correct()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = $this->_makeProductDataFake();
        $file = UploadedFile::fake()->create('image.xls');
        $makeFakeProDuctDataUpdate ['image'] = $file;
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['image' => 'The image field must be an image.']);
        $this->assertEquals($createFakeProDuctData->fresh()->image, $createFakeProDuctData->image);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_product()
    {
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = $this->_makeProductDataFake();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_update_product_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $createFakeProDuctData = $this->_createProductDataFake();
        $makeFakeProDuctDataUpdate = $this->_makeProductDataFake();
        $makeFakeProDuctDataUpdate['image'] = $this->_makeImageFile();
        $response = $this->putJson($this->_getRouteUpdate($createFakeProDuctData->id), $makeFakeProDuctDataUpdate);
        $response->assertForbidden();
    }
    private function _getRouteUpdate($id): string
    {
        return route('products.update', $id);
    }

    private function _makeProductDataFake(): array
    {
        return Product::factory()->make()->toArray();
    }

    private function _createProductDataFake()
    {
        return Product::factory()->create();
    }
    private function _makeImageFile(): File
    {
        Storage::fake('images');
        return UploadedFile::fake()->image('product.jpg');
    }
    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
