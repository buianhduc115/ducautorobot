<?php

namespace Tests\Feature\Products;


use App\Repositories\UserRepository;
use Tests\TestCase;
use App\Models\Product;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CreateProductTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test  */
    public function authenticated_user_can_create_product(): void
    {
        $this->_loginUserWithRole('superadmin');
        $countProductDataBefore = Product::count();
        $createFakeProductData = $this->_makeProductDataFake();
        $createFakeProductData['image'] = $this->_makeImageFile();
        $response = $this->postJson($this->_getRouteStore(), $createFakeProductData);
        $response->assertStatus(Response::HTTP_CREATED)
                ->assertJson([
                'status_code' => Response::HTTP_CREATED,
                'message' => 'created successfully.',
                ]);
        $countProductDataAfter = Product::count();
        $this->assertEquals($countProductDataBefore + 1, $countProductDataAfter);
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_product(): void
    {
        $createFakeProductData = $this->_makeProductDataFake();
        $createFakeProductData['image'] = $this->_makeImageFile();
        $response = $this->postJson($this->_getRouteStore(), $createFakeProductData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test  */
    public function authenticated_user_can_not_create_product_if_data_invalid(): void
    {
        $this->_loginUserWithRole('superadmin');
        $countProductDataBefore = Product::count();
        $createFakeProductData = Product::factory()->make(['name' => null])->toArray();
        $createFakeProductData['image'] = $this->_makeImageFile();
        $response = $this->postJson($this->_getRouteStore(), $createFakeProductData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'name',
            ],
        ]);
        $countProductDataAfter = Product::count();
        $this->assertEquals($countProductDataBefore , $countProductDataAfter);
    }

    public function authenticate_user_can_not_create_product_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $countProductDataBefore = Product::count();
        $createFakeProductData = $this->_makeProductDataFake();
        $createFakeProductData['image'] = $this->_makeImageFile();
        $response = $this->postJson($this->_getRouteStore(), $createFakeProductData);
        $response->assertForbidden();
        $countProductDataAfter = Product::count();
        $this->assertEquals($countProductDataBefore , $countProductDataAfter);
    }

    private function _makeImageFile(): File
    {
        Storage::fake('images');
        return UploadedFile::fake()->image('product.jpg');
    }
    private function _getRouteStore()
    {
        return route('products.store');
    }
    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _makeProductDataFake(): array
    {
        return Product::factory()->make()->toArray();
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
