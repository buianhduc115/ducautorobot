<?php

namespace Tests\Feature\Products;

use App\Repositories\UserRepository;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;

class DeleteProductTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function authenticated_user_can_delete_product_if_product_exist()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProductData = Product::factory()->create();
        $countProductDataBefore = Product::count();
        $response = $this->deleteJson($this->_getRouteDestroy($createFakeProductData->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('products', $createFakeProductData->toArray());
        $countProductDataAfter = Product::count();
        $this->assertEquals($countProductDataBefore - 1, $countProductDataAfter);
    }

    /** @test */
    public function authenticated_user_can_not_delete_product_if_product_not_exist()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->deleteJson($this->_getRouteDestroy(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_product()
    {
        $createFakeProductData = Product::factory()->create();
        $response = $this->deleteJson($this->_getRouteDestroy($createFakeProductData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_delete_product_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $createFakeProductData = Product::factory()->create();
        $countProductDataBefore = Product::count();
        $response = $this->deleteJson($this->_getRouteDestroy($createFakeProductData->id));
        $response->assertForbidden();
        $countProductDataAfter = Product::count();
        $this->assertEquals($countProductDataBefore , $countProductDataAfter);
    }

    private function _getRouteDestroy($id)
    {
        return route('products.destroy', $id);
    }
    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
