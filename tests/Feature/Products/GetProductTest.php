<?php

namespace Tests\Feature\Products;

use App\Repositories\UserRepository;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetProductTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function authenticated_user_can_get_products()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeProductData = Product::factory()->create();
        $response = $this->getJson($this->_getRouteShow($createFakeProductData->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'status_code',
                'data' => ['id', 'name', 'image', 'price', 'stock', 'descriptions', 'categories' => ['ids', 'names']],
            ]
        );
    }

    /** @test */
    public function authenticated_user_can_not_get_products_if_product_not_exits()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->getJson($this->_getRouteShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function authenticate_user_can_not_get_product_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $createFakeProductData = Product::factory()->create();
        $response = $this->getJson($this->_getRouteShow($createFakeProductData->id));
        $response->assertForbidden();
    }

    private function _getRouteShow($id): string
    {
        return route('products.show', $id);
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
