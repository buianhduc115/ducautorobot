<?php

namespace Tests\Feature\Roles;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function unauthenticated_user_can_not_see_all_role(): void
    {
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_super_admin_can_see_all_role(): void
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
    }

    /** @test */
    public function authenticate_user_can_not_see_all_role_if_user_has_not_permission(): void
    {
        $this->_loginUserWithRole('user');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function _getRouteIndex()
    {
        return route('roles.index');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
