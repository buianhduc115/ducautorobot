<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function unauthenticate_user_can_not_create_role_form(): void
    {
        $Response = $this->post($this->_getRouteStore());
        $Response->assertStatus(Response::HTTP_FOUND);
        $Response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_create_role_form_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->make()->toArray();
        $countRoleDataBefore = Role::count();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeRoleData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $createFakeRoleData);
        $response->assertRedirect(route('roles.index'));
        $countRoleDataAfter = Role::count();
        $this->assertEquals($countRoleDataBefore + 1, $countRoleDataAfter);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_field_name_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeRoleData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.create'));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_admin_has_not_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $createFakeRoleData = Role::factory()->make()->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeRoleData);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_user_has_not_permission(): void
    {
        $this->_loginUserWithRole('user');
        $createFakeRoleData = Role::factory()->make()->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeRoleData);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function _getRouteCreate()
    {
        return route('roles.create');
    }

    private function _getRouteStore()
    {
        return route('roles.store');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
