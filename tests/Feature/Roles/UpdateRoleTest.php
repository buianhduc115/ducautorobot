<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function unauthenticate_user_can_not_update_role_form(): void
    {
        $createFakeRoleData = Role::factory()->create();
        $response = $this->get($this->_getRouteEdit($createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_view_update_role_form(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->create();
        $response = $this->get($this->_getRouteEdit($createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
        $response->assertViewHas('role', $createFakeRoleData)
            ->assertSee($createFakeRoleData->name);
    }

    /** @test */
    public function authenticate_user_can_update_role_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->create();
        $makeFakeRoleDataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeRoleData->id))->put($this->_getRouteUpdate($createFakeRoleData->id), $makeFakeRoleDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $makeFakeRoleDataUpdate);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_admin_has_not_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $createFakeRoleData = Role::factory()->create();
        $makeFakeRoleDataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeRoleData->id))->put($this->_getRouteUpdate($createFakeRoleData->id), $makeFakeRoleDataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_user_has_not_permission(): void
    {
        $this->_loginUserWithRole('user');
        $createFakeRoleData = Role::factory()->create();
        $makeFakeRoleDataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeRoleData->id))->put($this->_getRouteUpdate($createFakeRoleData->id), $makeFakeRoleDataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_field_name_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->create();
        $makeFakeRoleDataUpdate  = Role::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeRoleData->id))->put($this->_getRouteUpdate($createFakeRoleData->id), $makeFakeRoleDataUpdate );
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteEdit($createFakeRoleData->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_role_is_not_exist(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = -1;
        $response = $this->get($this->_getRouteEdit($createFakeRoleData));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    private function _getRouteEdit($id)
    {
        return route('roles.edit', $id);
    }

    public function _getRouteUpdate($id)
    {
        return route('roles.update', $id);
    }

    public function _getRouteLogin()
    {
        return route('login');
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
