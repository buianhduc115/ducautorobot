<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function unauthenticate_user_can_not_delete_role(): void
    {
        $createFakeRoleData = Role::factory()->create();
        $response = $this->delete($this->_getRouteDestroy(  $createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_delete_role_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeRoleData = Role::factory()->create();
        $countRoleDataBefore = Role::count();
        $response = $this->delete($this->_getRouteDestroy($createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $countRoleDataAfter = Role::count();
        $this->assertEquals($countRoleDataBefore - 1, $countRoleDataAfter);
        $response->assertRedirect($this->_getRouteIndex());
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_admin_has_not_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $createFakeRoleData = Role::factory()->create();
        $response = $this->delete($this->_getRouteDestroy($createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_user_has_not_permission(): void
    {
        $this->_loginUserWithRole('user');
        $createFakeRoleData = Role::factory()->create();
        $response = $this->delete($this->_getRouteDestroy($createFakeRoleData->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_role_is_not_exist(): void
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->delete($this->_getRouteDestroy(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    private function _getRouteDestroy($id)
    {
        return route('roles.destroy', $id);
    }

    private function _getRouteLogin()
    {
        return route('login');
    }

    private function _getRouteIndex()
    {
        return route('roles.index');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
