<?php

namespace Tests\Feature\Categories;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;

class GetListCategorieTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }
    private function _getRouteIndex()
    {
        return route('categories.index');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
