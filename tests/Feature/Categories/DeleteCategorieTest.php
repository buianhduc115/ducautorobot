<?php

namespace Tests\Feature\Categories;

use App\Models\Categorie;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategorieTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function authenticated_user_can_delete_categories_if_category_exists()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $countCategoryDataBefore = Categorie::count();
        $response = $this->delete($this->_getRouteDestroy($createFakeCategoryData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('categories', $createFakeCategoryData->toArray());
        $countCategoryDataAfter = Categorie::count();
        $this->assertEquals($countCategoryDataBefore - 1, $countCategoryDataAfter);
        $response->assertRedirect($this->_getRouteIndex());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_categories()
    {
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $response = $this->delete($this->_getRouteDestroy($createFakeCategoryData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_categories_not_exists()
    {
        $this->_loginUserWithRole('superadmin');
        $id = -1;
        $response = $this->delete($this->_getRouteDestroy($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_delete_category_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $response = $this->delete($this->_getRouteDestroy($createFakeCategoryData->id));
        $response->assertForbidden();
    }
    private function _getCategoryDataFake()
    {
        return Categorie::factory()->create();
    }
    private function _getRouteDestroy($id)
    {
        return route('categories.destroy', $id);
    }

    private function _getRouteIndex()
    {
        route('categories.index');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
