<?php

namespace Tests\Feature\Categories;

use App\Models\Categorie;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategorieTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function authenticated_user_can_create_category_if_super_admin_has_permission()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $countCategoryDataBefore = Categorie::count();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeCategoryData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $createFakeCategoryData);
        $response->assertRedirect($this->_getRouteIndex());
        $countCategoryDataAfter = Categorie::count();
        $this->assertEquals($countCategoryDataBefore + 1, $countCategoryDataAfter);
    }

    /** @test */
    public function authenticated_user_can_create_category_if_admin_has_permission()
    {
        $this->_loginUserWithRole('admin');
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $countCategoryDataBefore = Categorie::count();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeCategoryData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $createFakeCategoryData);
        $response->assertRedirect($this->_getRouteIndex());
        $countCategoryDataAfter = Categorie::count();
        $this->assertEquals($countCategoryDataBefore + 1, $countCategoryDataAfter);
    }

    /** @test */
    public function authenticated_user_can_not_create_category_if_user_has_permission()
    {
        $this->_loginUserWithRole('user');
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeCategoryData);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticate_user_can_not_create_new_categories()
    {
        $createFakeCategoryData = $this->_getCategoryDataFake();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeCategoryData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_create_new_category_if_data_is_not_valid()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = Categorie::factory()->make(['name' => ''])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeCategoryData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteCreate());
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_see_create_new_category_form_if_super_admin_has_permission()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function authenticate_user_can_not_see_create_new_category_form_if_user_has_not_permission()
    {
        $this->_loginUserWithRole('user');
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function _getCategoryDataFake(): array
    {
        return Categorie::factory()->make()->toArray();
    }

    private function _getRouteCreate()
    {
        return route('categories.create');
    }

    private function _getRouteStore()
    {
        return route('categories.store');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }

    private function _getRouteIndex()
    {
        return route('categories.index');
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
