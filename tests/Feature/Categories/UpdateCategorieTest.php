<?php

namespace Tests\Feature\Categories;

use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Models\Categorie;
use App\Models\User;

class UpdateCategorieTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function authenticate_user_can_update_category_if_data_is_valid()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = $this->_getCreateCategoryDataFake();
        $makeFakeCategoryDataUpdate = $this->_getMakeCategoryDataFake();
        $response = $this->from($this->_getRouteEdit($createFakeCategoryData->id))->put($this->_getRouteUpdate($createFakeCategoryData->id), $makeFakeCategoryDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $makeFakeCategoryDataUpdate);
        $response->assertRedirect($this->_getRouteIndex());
    }

    /** @test */
    public function unauthenticate_user_can_not_update_category()
    {
        $createFakeCategoryData = $this->_getCreateCategoryDataFake();
        $makeFakeCategoryDataUpdate = $this->_getMakeCategoryDataFake();
        $response = $this->from($this->_getRouteEdit($createFakeCategoryData->id))->put($this->_getRouteUpdate($createFakeCategoryData->id), $makeFakeCategoryDataUpdate);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_see_update_category_form()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = $this->_getCreateCategoryDataFake();
        $response = $this->get($this->_getRouteEdit($createFakeCategoryData->id));
        $response->assertViewHas('category', $createFakeCategoryData);
        $response->assertSee($createFakeCategoryData->name, $createFakeCategoryData->parent_id);
    }

    /** @test */
    public function authenticate_user_can_not_update_category_if_data_is_not_valid()
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeCategoryData = $this->_getCreateCategoryDataFake();
        $makeFakeCategoryDataUpdate = Categorie::factory()->make(['name' => ''])->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeCategoryData->id))->put($this->_getRouteUpdate($createFakeCategoryData->id), $makeFakeCategoryDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteEdit($createFakeCategoryData->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_developer_if_category_is_not_exist()
    {
        $this->_loginUserWithRole('superadmin');
        $id = -1;
        $response = $this->get($this->_getRouteEdit($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    private function _getCreateCategoryDataFake()
    {
        return Categorie::factory()->create();
    }

    private function _getMakeCategoryDataFake(): array
    {
        return Categorie::factory()->make()->toArray();
    }

    private function _getRouteIndex()
    {
        return route('categories.index');
    }

    private function _getRouteEdit($id)
    {
        return route('categories.edit', $id);
    }

    private function _getRouteUpdate($id)
    {
        return route('categories.update', $id);
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
