<?php

namespace Tests\Feature\Authenticate;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;

class LoginTest extends TestCase
{
    /** @test  */
    public function authenticate_user_can_see_dashboard_if_login_successfully(): void
    {
        $credentials = [
            'email'    => 'user@deha-soft.com',
            'password' => '123456',
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteDashboard());
    }

    /** @test  */
    public function unauthenticated_user_can_not_login_with_account_if_email_is_invalid(): void
    {
        $credentials = [
            'email'    => null,
            'password' => '123456',
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test  */
    public function unauthenticated_user_can_not_login_with_account_if_password_is_invalid(): void
    {
        $credentials = [
            'email'    => 'user@deha-soft.com',
            'password' => null,
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }

    /** @test  */
    public function unauthenticated_user_can_not_login_with_account_if_data_is_invalid(): void
    {
        $credentials = [
            'email'    => null,
            'password' => null,
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email', 'password']);
    }

    /** @test  */
    public function unauthenticated_user_can_see_login_form(): void
    {
        $response = $this->get($this->_getRouteSignin());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auths.signin');
    }

    /** @test */
    public function unauthenticated_user_can_not_login_if_email_is_incorrect()
    {
        $credentials = [
            'email'    => 'use111r@deha-soft.com',
            'password' => '123456',
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['errorLogin' => 'The email or password you entered is incorrect.']);
    }

    /** @test */
    public function authenticated_user_can_not_login_if_email_is_not_valid()
    {
        $credentials = [
            'email'    => 'user@gmail.com',
            'password' => '123456',
        ];
        $response = $this->post($this->_getRouteSignin(), $credentials);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email' => 'The email is not valid (@deha-soft.com).']);
    }

    private function _getRouteSignin()
    {
        return route('login');
    }
    private function _getRouteDashboard()
    {
        return route('dashboard');
    }
}
