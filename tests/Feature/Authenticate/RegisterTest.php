<?php

namespace Tests\Feature\Authenticate;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @test  */
    public function unauthenticated_user_can_see_register_form(): void
    {
        $response = $this->get(route('signup'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auths.signup');
    }

    /** @test  */
    public function unauthenticated_user_can_register_new_user(): void
    {
        $createFakeRegistrationData = User::factory()->make()->toArray();
        $createFakeRegistrationData['avatar'] = $this->_makeImageFile();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getSigninRoute());
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_field_name_is_null(): void
    {
        $createFakeRegistrationData = User::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['name' => 'The name field is required.']);
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_field_email_is_null(): void
    {
        $createFakeRegistrationData = User::factory()->make(['email' => null])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['email'=> 'The email field is required.']);
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_user_email_is_exist(): void
    {
        $createFakeRegistrationData = User::factory()->make(['email' => 'superadmin@deha-soft.com'])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['email' => 'The email has already been taken.']);
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_field_birthday_is_null(): void
    {
        $createFakeRegistrationData = User::factory()->make(['birthday' => null])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['birthday' => 'The birthday field is required.']);
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_field_phone_is_null(): void
    {
        $createFakeRegistrationData = User::factory()->make(['phone' => null])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['phone'=> 'The phone field is required.']);
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_user_phone_is_exist(): void
    {
        $createFakeRegistrationData = User::factory()->make(['phone' => '+84 09123d2dd'])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['phone'=> 'The phone is not valid (Vietnamese network operator).']);
    }

    /** @test  */
    public function unauthenticated_user_can_not_register_new_user_if_field_address_is_null(): void
    {
        $createFakeRegistrationData = User::factory()->make(['address' => null])->toArray();
        $response = $this->post($this->_getRegistrationProcessRoute(), $createFakeRegistrationData);
        $response->assertSessionHasErrors(['address' => 'The address field is required.']);
    }
    private function _getSigninRoute()
    {
        return route('login');
    }
    private function _getRegistrationProcessRoute()
    {
        return route('registration_process');
    }
    private function _makeImageFile(): File
    {
        Storage::fake('images');
        return UploadedFile::fake()->image('avatar.jpg');
    }
}
