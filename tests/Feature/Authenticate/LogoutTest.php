<?php

namespace Tests\Feature\Authenticate;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function user_can_logout()
    {
        $this->_loginUserWithRole('user');
        $response = $this->get($this->_getRouteLogout());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
        $this->assertGuest();
    }

    private function _getRouteLogout()
    {
        return route('logout');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
