<?php

namespace Tests\Feature\Users;

use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;

class GetListUserTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function unauthenticated_user_can_not_view_all_user(): void
    {
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_view_all_user_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
    }

    /** @test */
    public function authenticate_user_can_view_all_user_if_user_has_permission(): void
    {
        $this->_loginUserWithRole('user');
        $response = $this->get($this->_getRouteIndex());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    private function _getRouteIndex()
    {
        return route('users.index');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
