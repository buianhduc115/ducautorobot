<?php

namespace Tests\Feature\Users;

use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Http\Response;


class CreateUserTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test  */
    public function unauthenticated_user_can_not_view_create_user_form(): void
    {
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_admin_has_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_user_has_permission(): void
    {
        $this->_loginUserWithRole('user');
        $response = $this->get($this->_getRouteCreate());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_create_new_user(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->make()->toArray();
        $createFakeUserData['avatar'] = $this->_makeImageFile();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeUserData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_name_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeUserData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteCreate());
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_user_email_is_exist(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = user::factory()->make(['email' => 'superadmin@deha-soft.com'])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeUserData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteCreate());
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_email_is_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->make(['email' => null])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeUserData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteCreate());
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_phone_is_not_Vietnam_phone(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->make(['phone' => '1231234568'])->toArray();
        $response = $this->from($this->_getRouteCreate())->post($this->_getRouteStore(), $createFakeUserData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteCreate());
        $response->assertSessionHasErrors(['phone']);
    }

    private function _getRouteCreate()
    {
        return route('users.create');
    }

    private function _getRouteStore()
    {
        return route('users.store');
    }

    private function _getRouteLogin()
    {
        return route('login');
    }
    private function _makeImageFile(): File
    {
        Storage::fake('images');
        return UploadedFile::fake()->image('avatar.jpg');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
