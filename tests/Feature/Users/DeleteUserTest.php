<?php

namespace Tests\Feature\Users;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }
    /** @test */
    public function unauthenticated_user_can_not_delete_user(): void
    {
        $createFakeUserData = User::factory()->create();
        $response = $this->delete($this->_getRouteDestroy($createFakeUserData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_delete_user_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $countUserDataBefore = User::count();
        $response = $this->delete($this->_getRouteDestroy($createFakeUserData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $countUserDataAfter = User::count();
        $this->assertEquals($countUserDataBefore - 1, $countUserDataAfter);
        $response->assertRedirect($this->_getRouteIndex());
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_admin_has_not_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $createFakeUserData  = User::factory()->create();
        $countUserDataBefore = User::count();
        $response = $this->delete($this->_getRouteDestroy($createFakeUserData->id));
        $countUserDataAfter = User::count();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertEquals($countUserDataBefore , $countUserDataAfter);
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_user_has_not_permission(): void
    {
        $this->_loginUserWithRole('user');
        $createFakeUserData = User::factory()->create();
        $countUserDataBefore = User::count();
        $response = $this->delete($this->_getRouteDestroy($createFakeUserData->id));
        $countUserDataAfter = User::count();
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertEquals($countUserDataBefore , $countUserDataAfter);
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_user_not_exist(): void
    {
        $this->_loginUserWithRole('superadmin');
        $userID = -1;
        $response = $this->delete($this->_getRouteDestroy($userID));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    private function _getRouteDestroy($id)
    {
        return route('users.destroy', $id);
    }

    private function _getRouteLogin()
    {
        return route('login');
    }

    private function _getRouteIndex()
    {
        return route('users.index');
    }
    protected function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
