<?php

namespace Tests\Feature\Users;

use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;


class UpdateUserTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test */
    public function unauthenticated_user_can_not_view_update_user_form(): void
    {
        $createFakeUserData = User::factory()->create();
        $response = $this->get($this->_getRouteEdit($createFakeUserData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_view_update_user_form(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $response = $this->get($this->_getRouteEdit($createFakeUserData->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
        $response->assertViewHas('user', $createFakeUserData);
    }

    /** @test */
    public function authenticated_user_can_not_see_view_update_user_form_if_user_is_not_exist(): void
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteEdit(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_update_user_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $makeFakeUserDataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeUserData->id))->put($this->_getRouteUpdate($createFakeUserData->id), $makeFakeUserDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteShow($createFakeUserData->id));
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_admin_has_not_permission(): void
    {
        $this->_loginUserWithRole('admin');
        $createFakeUserData = User::factory()->create();
        $makeFakeUserDataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeUserData->id))->put($this->_getRouteUpdate($createFakeUserData->id), $makeFakeUserDataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_name_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $makeFakeUserDataUpdate = User::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeUserData->id))->put($this->_getRouteUpdate($createFakeUserData->id), $makeFakeUserDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteEdit($createFakeUserData->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_phone_number_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $makeFakeUserDataUpdate = User::factory()->make(['phone' => null])->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeUserData->id))->put($this->_getRouteUpdate($createFakeUserData->id), $makeFakeUserDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteEdit($createFakeUserData->id));
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_email_number_null(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $makeFakeUserDataUpdate = User::factory()->make(['email' => null])->toArray();
        $response = $this->from($this->_getRouteEdit($createFakeUserData->id))->put($this->_getRouteUpdate($createFakeUserData->id), $makeFakeUserDataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteEdit($createFakeUserData->id));
        $response->assertSessionHasErrors(['email']);
    }

    private function _getRouteEdit($id)
    {
        return route('users.edit', $id);
    }

    private function _getRouteUpdate($id)
    {
        return route('users.update', $id);
    }

    private function _getRouteLogin()
    {
        return route('login');
    }

    private function _getRouteShow($id)
    {
        return route('users.show', $id);
    }
    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
