<?php

namespace Tests\Feature\Users;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Illuminate\Http\Response;

class GetUserTest extends TestCase
{
    protected UserRepository $userRepository;
    public function setUp():void
    {
        parent::setUp();
        $this->userRepository = new UserRepository();
    }

    /** @test  */
    public function unauthenticated_user_can_not_information_user(): void
    {
        $createFakeUserData = User::factory()->create();
        $response = $this->get($this->_getRouteinformation( $createFakeUserData->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->_getRouteLogin());
    }

    /** @test  */
    public function authenticated_user_can_get_user(): void
    {
        $user = $this->_loginUserWithRole('admin');
        $response = $this->get($this->_getRouteinformation($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.information');
        $response->assertViewHas('user', $user);
    }

    /** @test */
    public function authenticated_user_can_show_user_if_super_admin_has_permission(): void
    {
        $this->_loginUserWithRole('superadmin');
        $createFakeUserData = User::factory()->create();
        $response = $this->get($this->_getRouteShow($createFakeUserData->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
        $response->assertViewHas('user', $createFakeUserData)
            ->assertSee($createFakeUserData->id);
    }

    /** @test */
    public function authenticated_user_can_not_get_user_if_user_not_exist()
    {
        $this->_loginUserWithRole('superadmin');
        $response = $this->get($this->_getRouteShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    private function _getRouteinformation($id)
    {
        return route('users.information', $id);
    }

    private function _getRouteShow($id)
    {
        return route('users.show', $id);
    }
    private function _getRouteLogin()
    {
        return route('login');
    }

    private function _loginUserWithRole(string $role)
    {
        $user = $this->userRepository->createUserWithRole($role);
        $this->actingAs($user);
        return $user;
    }
}
