<?php

namespace Database\Seeders;

use App\Models\Silder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SilderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Silder::factory(5)->create();
    }
}
