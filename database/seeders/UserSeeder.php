<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdmin = User::create([
            'id' => 1,
            'name' => 'Super Admin',
            'birthday' => '03-03-1999',
            'email' => 'superadmin@deha-soft.com',
            'phone' => '0931234568',
            'password' => '123456',
            'address' => '789 Oak Street, San Francisco, CA 94103',
        ]);
        $superAdmin->roles()->attach(1);

        $admin = User::create([
            'id' => 2,
            'name' => 'Admin',
            'birthday' => '03-03-1999',
            'email' => 'admin@deha-soft.com',
            'phone' => '0921234567',
            'password' => '123456',
            'address' => '456 Elm Street, San Francisco, CA 94102',
        ]);
        $admin->roles()->attach(2);

        $user = User::create([
            'id' => 3,
            'name' => 'User',
            'birthday' => '03-03-1999',
            'email' => 'user@deha-soft.com',
            'phone' => '0912345678',
            'password' => '123456',
            'address' => '123 Main Street, San Francisco, CA 94101',
        ]);
        $user->roles()->attach(3);
    }
}
