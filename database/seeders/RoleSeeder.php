<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdminRole = Role::create([
            'id' => 1,
            'name' => 'superadmin',
        ]);
        $superAdminRole->permissions()->attach([1]);

        $adminRole = Role::create([
            'id' => 2,
            'name' => 'admin',
        ]);
        $adminRole->permissions()->attach([2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 16, 19, 23, 26,30]);

        $user = Role::create([
            'id' => 3,
            'name' => 'user',
        ]);
        $user->permissions()->attach([2, 5, 9, 12,27, 28,30]);
    }
}
