<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            [
                'id' => '1',
                'name' => 'all',
                'route_name' => 'all',
            ],
            [
                'id' => '2',
                'name' => 'products.index',
                'route_name' => 'products.index',
            ],
            [
                'id' => '3',
                'name' => 'products.create',
                'route_name' => 'products.create',
            ],
            [
                'id' => '4',
                'name' => 'products.store',
                'route_name' => 'products.store',
            ],
            [
                'id' => '5',
                'name' => 'products.show',
                'route_name' => 'products.show',
            ],
            [
                'id' => '6',
                'name' => 'products.edit',
                'route_name' => 'products.edit',
            ],
            [
                'id' => '7',
                'name' => 'products.update',
                'route_name' => 'products.update',
            ],
            [
                'id' => '8',
                'name' => 'products.destroy',
                'route_name' => 'products.destroy',
            ],
            [
                'id' => '9',
                'name' => 'categories.index',
                'route_name' => 'categories.index',
            ],
            [
                'id' => '10',
                'name' => 'categories.create',
                'route_name' => 'categories.create',
            ],
            [
                'id' => '11',
                'name' => 'categories.store',
                'route_name' => 'categories.store',
            ],
            [
                'id' => '12',
                'name' => 'categories.show',
                'route_name' => 'categories.show',
            ],
            [
                'id' => '13',
                'name' => 'categories.edit',
                'route_name' => 'categories.edit',
            ],
            [
                'id' => '14',
                'name' => 'categories.update',
                'route_name' => 'categories.update',
            ],
            [
                'id' => '15',
                'name' => 'categories.destroy',
                'route_name' => 'categories.destroy',
            ],
            [
                'id' => '16',
                'name' => 'roles.index',
                'route_name' => 'roles.index',
            ],
            [
                'id' => '17',
                'name' => 'roles.create',
                'route_name' => 'roles.create',
            ],
            [
                'id' => '18',
                'name' => 'roles.store',
                'route_name' => 'roles.store',
            ],
            [
                'id' => '19',
                'name' => 'roles.show',
                'route_name' => 'roles.show',
            ],
            [
                'id' => '20',
                'name' => 'roles.edit',
                'route_name' => 'roles.edit',
            ],
            [
                'id' => '21',
                'name' => 'roles.update',
                'route_name' => 'roles.update',
            ],
            [
                'id' => '22',
                'name' => 'roles.destroy',
                'route_name' => 'roles.destroy',
            ],
            [
                'id' => '23',
                'name' => 'users.index',
                'route_name' => 'users.index',
            ],
            [
                'id' => '24',
                'name' => 'users.create',
                'route_name' => 'users.create',
            ],
            [
                'id' => '25',
                'name' => 'users.store',
                'route_name' => 'users.store',
            ],
            [
                'id' => '26',
                'name' => 'users.show',
                'route_name' => 'users.show',
            ],
            [
                'id' => '27',
                'name' => 'users.edit',
                'route_name' => 'users.edit',
            ],
            [
                'id' => '28',
                'name' => 'users.update',
                'route_name' => 'users.update',
            ],
            [
                'id' => '29',
                'name' => 'users.destroy',
                'route_name' => 'users.destroy',
            ],
            [
                'id' => '30',
                'name' => 'users.information',
                'route_name' => 'users.information',
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
