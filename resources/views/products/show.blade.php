<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sản phẩm</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Đóng"></button>
            </div>
            <div class="modal-body">
                <form data-url="" data-action-name="" method="POST" class="product-data form-product-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">Tên</label>
                                <input type="text" name="name" class="form-control product-data input name-product">
                                <p class='name-error text-danger inputerror error'></p>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Danh mục</label>
                                <select name="categories[]" class="form-control product-data select select-categories" multiple>
                                </select>
                                <p class='categories-error text-danger inputerror error'></p>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Giá</label>
                                <input type="text" name="price" class="form-control product-data input price-product">
                                <p class='price-error text-danger inputerror error'></p>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Tồn kho</label>
                                <input type="number" min="0" name="stock" class="form-control product-data input stock-product">
                                <p class='stock-error text-danger inputerror error'></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 image-input">
                                <label class="form-label">Hình ảnh</label>
                                <input type="file" name="image" class="form-control product-data input input-image">
                                <p class='image-error text-danger inputerror error'></p>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-6 n-image">
                                    <label class="form-label">Hình ảnh mới</label>
                                    <img class="image-preview img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png" alt="">
                                </div>
                                <div class="col-6 hidden-image">
                                    <label class="form-label">Hình ảnh cũ</label>
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png" class="o-image img-fluid" alt="">
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Mô tả</label>
                                <textarea name="descriptions" class="form-control product-data textarea descriptions-product" rows="5"></textarea>
                                <p class='descriptions-error text-danger inputerror error'></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-close" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary btn-save">Lưu</button>
            </div>
        </div>
    </div>
</div>
