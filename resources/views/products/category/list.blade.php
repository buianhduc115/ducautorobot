@extends('fontend.layouts.master')

@section('content')
    @include('fontend.components.silder')
    <!-- Cửa Hàng Bắt Đầu -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <!-- Sidebar Cửa Hàng Bắt Đầu -->
            <div class="col-lg-3 col-md-12">
                <!-- Lọc Theo Giá Bắt Đầu -->
                <div class="border-bottom mb-4 pb-4">
                    <h5 class="font-weight-semi-bold mb-4">Lọc theo giá</h5>
                    <form id="filterForm" action="{{ route('category.product.list', ['id' => $categoryId]) }}" method="GET">
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input price-range" id="price-1" name="price_range[]" value="price-1">
                            <label class="custom-control-label" for="price-1">0 VNĐ - 100,000 VNĐ</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input price-range" id="price-2" name="price_range[]" value="price-2">
                            <label class="custom-control-label" for="price-2">100,000 VNĐ - 200,000 VNĐ</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input price-range" id="price-3" name="price_range[]" value="price-3">
                            <label class="custom-control-label" for="price-3">200,000 VNĐ - 300,000 VNĐ</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input price-range" id="price-4" name="price_range[]" value="price-4">
                            <label class="custom-control-label" for="price-4">300,000 VNĐ - 400,000 VNĐ</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input price-range" id="price-5" name="price_range[]" value="price-5">
                            <label class="custom-control-label" for="price-5">400,000 VNĐ trở lên</label>
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Áp dụng</button>
                        <button type="button" id="clearFilter" class="btn btn-link mt-3 ml-3">Xoá lọc</button>
                    </form>
                </div>
                <!-- Lọc Theo Giá Kết Thúc -->
                <div class="border-bottom mb-4 pb-4">
                    <h5 class="font-weight-semi-bold mb-4">Lọc theo tuổi</h5>
                    <form action="{{ route('category.product.list', ['id' => $categoryId]) }}" method="GET">
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input" id="age-1" name="age_range" value="1-3" {{ request()->input('age_range') == '1-3' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="age-1">1 - 3 Tuổi</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input" id="age-2" name="age_range" value="3-6" {{ request()->input('age_range') == '3-6' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="age-2">3 - 6 Tuổi</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input" id="age-3" name="age_range" value="6-12" {{ request()->input('age_range') == '6-12' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="age-3">6 - 12 Tuổi</label>
                        </div>
                        <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                            <input type="radio" class="custom-control-input" id="age-4" name="age_range" value="12+" {{ request()->input('age_range') == '12+' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="age-4">12 Tuổi trở lên</label>
                        </div>
                        <button type="submit" class="btn btn-primary mt-3">Áp dụng</button>
                        <button type="button" id="clearAgeFilter" class="btn btn-link mt-3 ml-3">Xoá lọc</button>
                    </form>
                </div>
            </div>

            <div class="col-lg-9 col-md-12">
                <div class="row pb-3">
                    <div class="col-12 pb-1">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <form action="{{ route('category.product.list', ['id' => $categoryId]) }}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Tìm kiếm theo tên" name="name" value="{{ request()->get('name') }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text bg-transparent text-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if ($products->isEmpty())
                        <div class="col-12">
                            <p class="text-center" style="color: red">Không có sản phẩm nào.</p>
                        </div>
                    @else
                        @foreach($products as $product)
                            <div class="col-lg-4 col-md-6 col-sm-12 pb-1">
                                <div class="card product-item border-0 mb-4">
                                    <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                        <img class="img-fluid w-100" src="{{$product->image}}" alt="">
                                    </div>
                                    <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                        <h6 class="text-truncate mb-3">{{$product->name}}</h6>
                                        <h6 class="text-truncate mb-3">{{$product->age_range}} Tuổi</h6>
                                        <div class="d-flex justify-content-center">
                                            <h6>{{number_format($product->price)}} VNĐ</h6>
                                        </div>
                                    </div>
                                    <div class="card-footer d-flex justify-content-between bg-light border">
                                        <a href="{{ route('products.showDetail', $product->id) }}" class="btn btn-sm text-dark p-0">
                                            <i class="fas fa-eye text-primary mr-1"></i>Xem chi tiết
                                        </a>
                                        @if ($product->stock > 0)
                                            <form action="{{ route('client.carts.add') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                <input type="hidden" name="quantity" value="1">
                                                <button type="submit" class="btn btn-sm text-dark p-0">
                                                    <i class="fas fa-shopping-cart text-primary mr-1"></i> Thêm vào giỏ hàng
                                                </button>
                                            </form>
                                        @else
                                            <span class="text-danger">Hết hàng</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-12" style="display: grid; place-items: center;">
                            {{ $products->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        document.addEventListener('DOMContentLoaded', function() {

            var selectedPriceRange = localStorage.getItem('selectedPriceRange');
            if (selectedPriceRange) {
                document.querySelector('input[value="' + selectedPriceRange + '"]').checked = true;
            }

            var radios = document.querySelectorAll('.price-range');
            radios.forEach(function(radio) {
                radio.addEventListener('change', function() {
                    var selectedValue = this.value;
                    localStorage.setItem('selectedPriceRange', selectedValue);
                });
            });

            document.getElementById('clearFilter').addEventListener('click', function() {
                localStorage.removeItem('selectedPriceRange');
                document.getElementById('filterForm').reset();
                document.getElementById('filterForm').submit();
            });

            var selectedAgeRange = localStorage.getItem('selectedAgeRange');
            if (selectedAgeRange) {
                document.querySelector('input[value="' + selectedAgeRange + '"]').checked = true;
            }

            var ageRadios = document.querySelectorAll('.age-range');
            ageRadios.forEach(function(radio) {
                radio.addEventListener('change', function() {
                    var selectedValue = this.value;
                    localStorage.setItem('selectedAgeRange', selectedValue);
                });
            });

            document.getElementById('clearAgeFilter').addEventListener('click', function() {
                localStorage.removeItem('selectedAgeRange');
                document.getElementById('filterForm').submit();
            });

        });
    </script>
@endsection
