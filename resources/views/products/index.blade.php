@extends('layouts.main')

@section('contents')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header">
                        <h5 class="card-title">Tìm kiếm sản phẩm</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('products.index')}}" class="search-form" id="index" data-url="{{route('products.list')}}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label class="form-label">Tên sản phẩm</label>
                                    <input type="text" class="form-control name" name="name" value="{{ request('name') }}">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label">Danh mục</label>
                                    <select class="form-control category" name="category" id="category">
                                        <option value="">Chọn danh mục...</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label"></label>
                                    <button type="button" class="btn btn-primary btn-search">Tìm kiếm
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @hasPermission('products.create', 'products.store')
                <div class="card my-4">
                    <div class=" me-3 my-3 text-end">
                        <button type="button" class="btn bg-gradient-dark mb-0 product-action" data-bs-toggle="modal" data-bs-target="#exampleModal" data-url="{{ route('products.store')}}" data-action-name="create"><i
                                class="material-icons text-sm">add</i>&nbsp;&nbsp;Thêm sản phẩm mới</button>
                    </div>
                    @endhasPermission
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table table-bordered table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Tên</th>
                                    <th class="text-center">Giá</th>
                                    <th class="text-center">Danh mục</th>
                                    <th class="text-center">Hình ảnh</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody class="table-body">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            @include('products.show')
        </div>
        <ul class="pagination">
        </ul>
    </div>
@endsection
