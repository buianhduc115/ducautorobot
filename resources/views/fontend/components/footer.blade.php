<!-- Footer Start -->
<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
    <div class="row px-xl-5 pt-5">
        <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
            <a href="" class="text-decoration-none">
                <h1 class="mb-4 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border border-white px-3 mr-1">ĐỨC AUTO ROBOT</span>Shop</h1>
            </a>
            <p>Mang đến niềm vui và sáng tạo với những robot đồ chơi thông minh và hiện đại nhất.</p>
            <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>275 P. Tây Sơn, Ngã Tư Sở, Đống Đa, Hà Nội</p>
            <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>buianhduc@ducautorobot.com</p>
            <p class="mb-0"><i class="fa fa-phone-alt text-primary mr-3"></i>0932456789</p>
        </div>
    </div>
    <div class="row border-top border-light mx-xl-5 py-4">
        <div class="col-md-6 px-xl-0">
            <p class="mb-md-0 text-center text-md-left text-dark">
                Phân phối bởi <a href="" target="_blank">ĐỨC AUTO ROBOT</a>
            </p>
        </div>
    </div>
</div>
<!-- Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
