@extends('fontend.layouts.master')

@section('content')
    @include('fontend.components.silder')
    @if (session('success'))
        <div id="success-message" class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="row px-xl-5">
        <div class="col-lg-8 table-responsive mb-5">
            <table class="table table-bordered text-center mb-0">
                <thead class="bg-secondary text-dark">
                <tr>
                    <th>Sản phẩm</th>
                    <th>Giá</th>
                    <th>Số lượng</th>
                    <th>Tồn kho</th>
                    <th>Tổng</th>
                    <th>Hành động</th>
                </tr>
                </thead>
                <tbody class="align-middle">
                @forelse ($cart->products as $item)
                    <tr id="row-{{ $item->id }}">
                        <td class="align-middle">
                            <img src="{{ $item->product->image_path }}" alt=""
                                 style="width: 50px;">
                            {{ $item->product->name }}
                        </td>
                        <td class="align-middle">
                            <p>
                                ${{ $item->product->price }}
                            </p>

                            @if ($item->product->sale)
                                <p>
                                    ${{ $item->product->sale_price }}
                                </p>
                            @endif
                        </td>
                        <td class="align-middle">
                            <div class="input-group quantity mx-auto" style="width: 100px;">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-minus btn-update-quantity"
                                            data-action="{{ route('client.carts.update_product_quantity', $item->id) }}"
                                            data-id="{{ $item->id }}">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <input type="number"
                                       class="form-control form-control-sm bg-secondary text-center p-0"
                                       id="productQuantityInput-{{ $item->id }}" min="1"
                                       value="{{ $item->product_quantity }}">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-plus btn-update-quantity"
                                            data-action="{{ route('client.carts.update_product_quantity', $item->id) }}"
                                            data-id="{{ $item->id }}">

                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td class="align-middle">
                            {{ $item->product->stock }}
                        </td>
                        <td class="align-middle">
                            <span id="cartProductPrice{{ $item->id }}">
                                ${{ $item->product->price * $item->product_quantity }}
                            </span>
                        </td>
                        <td class="align-middle">
                            <button class="btn btn-sm btn-primary btn-remove-product"
                                    data-action="{{ route('client.carts.remove_product', $item->id) }}">
                                <i class="fa fa-times"></i>
                            </button>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">Giỏ hàng của bạn trống</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="col-lg-4">
            <form class="mb-5" method="POST" action="{{ route('client.carts.apply_coupon') }}">
                @csrf
                <div class="input-group">
                    <input type="text" class="form-control p-4" value="{{ Session::get('coupon_code') }}"
                           name="coupon_code" placeholder="Mã giảm giá">
                    <div class="input-group-append">
                        <button class="btn btn-primary">Áp dụng mã giảm giá</button>
                    </div>
                </div>
            </form>

            <div class="card border-secondary mb-5">
                <div class="card-header bg-secondary border-0">
                    <h4 class="font-weight-semi-bold m-0">Tóm tắt giỏ hàng</h4>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3 pt-1">
                        <h6 class="font-weight-medium">Tổng cộng</h6>
                        <h6 class="font-weight-medium total-price" data-price="{{ $cart->total_price }}">
                            {{ number_format($cart->total_price, 0, ',', '.') }} ₫
                        </h6>
                    </div>

                    @if (session('discount_amount_price'))
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Mã giảm giá</h6>
                            <h6 class="font-weight-medium coupon-div" data-price="{{ session('discount_amount_price') }}">
                                {{ number_format(session('discount_amount_price'), 0, ',', '.') }} %
                            </h6>
                        </div>
                    @endif
                </div>
                <div class="card-footer border-secondary bg-transparent">
                    <div class="d-flex justify-content-between mt-2">
                        <h5 class="font-weight-bold">Tổng thanh toán</h5>
                        <h5 class="font-weight-bold total-price-all"></h5>
                    </div>
                    @if ($cart->products->count() > 0)
                        <a href="{{ route('client.checkout.index') }}"
                           class="btn btn-block btn-primary my-3 py-3">Tiến hành thanh toán</a>
                    @else
                        <button class="btn btn-block btn-secondary my-3 py-3" disabled>Giỏ hàng trống</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('client/cart/cart.js') }}"></script>
@endsection
