@extends('fontend.layouts.master')

@section('content')
    @if(session('success'))
        <div class="alert alert-info">
            {{ session('success') }}
        </div>
    @endif
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="container-fluid pt-5">
        <form id="checkout-form" class="row px-xl-5" method="POST" action="{{ route('client.checkout.proccess') }}">
            @csrf
            <div class="col-lg-8">
                <div class="mb-4">
                    <h4 class="font-weight-semi-bold mb-4">Địa chỉ thanh toán</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Họ và tên</label>
                            <input class="form-control" value="{{ old('customer_name') }}" name="customer_name"
                                   type="text" placeholder="ABC">
                            @error('customer_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror()

                        </div>

                        <div class="col-md-6 form-group">
                            <label>Email</label>
                            <input class="form-control" name="customer_email" value="{{ old('customer_email') }}"
                                   type="text" placeholder="example@email.com">
                            @error('customer_email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror()
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Số điện thoại</label>
                            <input class="form-control" name="customer_phone" value="{{ old('customer_phone') }}"
                                   type="text" placeholder="+123 456 789">
                            @error('customer_phone')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror()
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Địa chỉ</label>
                            <input class="form-control" name="customer_address" value="{{ old('customer_address') }}"
                                   type="text" placeholder="123 đường">
                            @error('customer_address')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror()
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Ghi chú</label>
                            <input class="form-control" value="{{ old('note') }}" name="note" type="text"
                                   placeholder="Ghi chú">
                            @error('note')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror()
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Tổng đơn hàng</h4>
                    </div>
                    <div class="card-body">
                        <h5 class="font-weight-medium mb-3">Sản phẩm</h5>
                        @foreach ($cart->products as $item)
                            <div class="d-flex justify-content-between">
                                <p> {{ $item->product_quantity }} x {{ $item->product->name }}</p>
                                <p
                                    style="{{ $item->product->sale ? 'text-decoration: line-through' : '' }}">
                                    {{ number_format($item->product_quantity * $item->product->price, 0, ',', '.') }} VNĐ
                                </p>

                            </div>
                        @endforeach
                        <hr class="mt-0">
                        <div class="d-flex justify-content-between mb-3 pt-1">
                            <h6 class="font-weight-medium">Tổng tiền hàng</h6>
                            <h6 class="font-weight-medium">
                                {{ number_format($cart->total_price, 0, ',', '.') }} VNĐ
                            </h6>

                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Phí vận chuyển</h6>
                            <h6 class="font-weight-medium">35000 VNĐ</h6>
                            <input type="hidden" value="35000" name="ship">

                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Mã giảm giá</h6>
                            <h6 class="font-weight-medium coupon-div"
                                data-price="{{ session('discount_amount_price') }}">
                                {{ number_format(session('discount_amount_price'), 0, ',', '.') }} VNĐ
                            </h6>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <div class="d-flex justify-content-between mt-2">
                            <h5 class="font-weight-bold">Tổng thanh toán</h5>
                            <h5 class="font-weight-bold total-price-all"></h5>
                            <input type="hidden" id="total" value="" name="total">
                        </div>
                    </div>
                </div>
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Phương thức thanh toán</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" value="cash_on_delivery" name="payment" id="cash_on_delivery" checked>
                                <label class="custom-control-label" for="cash_on_delivery">Thanh toán khi nhận hàng</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" value="vnpay" name="payment" id="vnpay" >
                                <label class="custom-control-label" for="vnpay">Thanh toán qua VNPay</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <button type="submit" class="btn btn-lg btn-block btn-primary font-weight-bold my-3 py-3">Đặt hàng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            getTotalValue();

            function getTotalValue() {
                let total = {{ $cart->total_price }};
                let couponPrice = $('.coupon-div')?.data('price') ?? 0;
                let shipping = 35000;
                let totalPrice = total + shipping - couponPrice;

                $('.total-price-all').text(`{{ number_format($cart->total_price + 35000 - session('discount_amount_price'), 0, ',', '.') }} VNĐ`);

                $('#total').val(totalPrice);
            }

            $('input[name="payment"]').on('change', function() {
                let paymentMethod = $(this).val();
                let form = $('#checkout-form');
                if (paymentMethod === 'vnpay') {
                    form.attr('action', "{{ route('vnpay.create') }}");
                } else {
                    form.attr('action', "{{ route('client.checkout.proccess') }}");
                }
            });
        });
    </script>
@endsection

