@extends('layouts.main')

@section('contents')
    <h3 class="mb-4"> Đơn hàng </h3>

    <div class="container-fluid pt-5">

        <div class="col card">
            <div>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Trạng thái</th>
                        <th>Tổng tiền</th>
                        <th>Phí vận chuyển</th>
                        <th>Tên khách hàng</th>
                        <th>Email khách hàng</th>
                        <th>Địa chỉ khách hàng</th>
                        <th>Ghi chú</th>
                        <th>Phương thức thanh toán</th>
                        <th>Mã Thanh Toán</th>
                    </tr>

                    @foreach ($orders as $item)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>

                                <div class="input-group input-group-static mb-4">
                                    <select name="status" class="form-control select-status"
                                            data-action="{{ route('admin.orders.update_status', $item->id) }}">
                                        @foreach (config('order.status') as $status)
                                            <option value="{{ $status }}"
                                                {{ $status == $item->status ? 'selected' : '' }}>{{ $status }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td>${{ $item->total }}</td>

                            <td>${{ $item->ship }}</td>
                            <td>{{ $item->customer_name }}</td>
                            <td>{{ $item->customer_email }}</td>

                            <td>{{ $item->customer_address }}</td>
                            <td>{{ $item->note }}</td>
                            <td>{{ $item->payment }}</td>
                            <td>{{ $item->vnp_TxnRef }}</td>
                        </tr>
                    @endforeach
                </table>
                {{ $orders->links() }}
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/order/order.js') }}"></script>
@endsection
