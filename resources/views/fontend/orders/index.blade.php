@extends('fontend.layouts.master')

@section('content')
    @include('fontend.components.silder')
    <div class="container-fluid pt-5">
        @if (session('success'))
            <h1 class="text-primary">{{ session('success') }}</h1>
        @endif

        <div class="col">
            <div>
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Trạng thái</th>
                        <th>Tổng cộng</th>
                        <th>Phí vận chuyển</th>
                        <th>Tên khách hàng</th>
                        <th>Email khách hàng</th>
                        <th>Địa chỉ khách hàng</th>
                        <th>Ghi chú</th>
                        <th>Thanh toán</th>
                    </tr>

                    @foreach ($orders as $index => $item)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $item->status }}</td>
                            <td>{{ number_format($item->total) }} VNĐ</td>
                            <td>{{ number_format($item->ship) }} VNĐ</td>
                            <td>{{ $item->customer_name }}</td>
                            <td>{{ $item->customer_email }}</td>
                            <td>{{ $item->customer_address }}</td>
                            <td>{{ $item->note }}</td>
                            <td>{{ $item->payment }}</td>
                            <td>
                                @if ($item->status == 'pending')
                                    <form action="{{ route('client.orders.cancel', $item->id) }}" id="form-cancel{{ $item->id }}" method="post">
                                        @csrf
                                        <button class="btn btn-cancel btn-danger" data-id={{ $item->id }}>Hủy đơn hàng</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $(document).on("click", ".btn-cancel", function(e) {
                e.preventDefault();
                let id = $(this).data("id");
                confirmDelete()
                    .then(function() {
                        $(`#form-cancel${id}`).submit();
                    })
                    .catch();
            });
        });
    </script>
@endsection
