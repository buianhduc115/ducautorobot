@extends('fontend.layouts.master')

@section('content')
    @include('fontend.components.silder')
    @if (session('message'))
        <h2 class="" style="text-align: center; width:100%; color:red"> {{ session('message') }}</h2>
    @endif

    <div class="container-fluid py-5">
        <form action="{{ route('client.carts.add') }}" method="POST" class="row px-xl-5">
            @csrf
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <div class="col-lg-5 pb-5">
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner border">
                        <div class="carousel-item active">
                            <img class="img-fluid w-100" src="{{$product->image}}" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 pb-5">
                <h3 class="font-weight-semi-bold">{{ $product->name }}</h3>
                <div class="d-flex mb-3"></div>
                <h3 class="font-weight-semi-bold mb-4">{{ number_format($product->price, 0, ',', '.') }} VNĐ</h3>
                <h6 class="text-truncate mb-3">{{$product->age_range}} Tuổi</h6>

                <div class="d-flex mb-4">
                    @if ($product->stock > 0)
                        <p class="text-dark font-weight-medium mb-0 mr-3">Còn Lại: {{ $product->stock }}</p>
                    @else
                        <p class="text-danger mb-0">Hết hàng</p>
                    @endif
                </div>
                @if ($product->stock > 0)
                    <div class="d-flex align-items-center mb-4 pt-2">
                        <div class="input-group quantity mr-3" style="width: 130px;">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-minus">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <input type="number" id="quantity-input" class="form-control bg-secondary text-center" name="quantity" value="1" min="1" max="{{ $product->stock }}">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-plus">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <button id="add-to-cart-button" class="btn btn-primary px-3"><i class="fa fa-shopping-cart mr-1"></i> Thêm vào giỏ hàng</button>
                    </div>
                @endif
            </div>
        </form>
        <div class="row px-xl-5">
            <div class="col">
                <div class="nav nav-tabs justify-content-center border-secondary mb-4">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#tab-pane-1">Mô tả</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb3">Mô tả sản phẩm</h4>
                        {!! $product->descriptions !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
