<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <div class="navbar-brand m-0">
            <img src="{{ asset('assets/img/logos/logoweb.png') }}" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-white">Xin chào
            @auth
                    {{ auth()->user()->name }}
                @else
                    Khách
                @endauth
            </span>
        </div>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main">
        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('dashboard')) active bg-gradient-primary @endif" href="{{ route('dashboard') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text ms-1">Bảng điều khiển</span>
                </a>
            </li>

            @hasPermission('users.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('users.*')) active bg-gradient-primary @endif" href="{{ route('users.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">view_headline</i>
                    </div>
                    <span class="nav-link-text ms-1">Quản lý người dùng</span>
                </a>
            </li>
            @endhasPermission

            @hasPermission('products.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('products.*')) active bg-gradient-primary @endif" href="{{ route('products.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">production_quantity_limits</i>
                    </div>
                    <span class="nav-link-text ms-1">Sản phẩm</span>
                </a>
            </li>
            @endhasPermission

            @hasPermission('categories.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('categories.*')) active bg-gradient-primary @endif" href="{{ route('categories.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">table_view</i>
                    </div>
                    <span class="nav-link-text ms-1">Danh mục</span>
                </a>
            </li>
            @endhasPermission

            @hasPermission('roles.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('roles.*')) active bg-gradient-primary @endif" href="{{ route('roles.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Vai trò</span>
                </a>
            </li>
            @endhasPermission

            @hasPermission('silders.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('silders.*')) active bg-gradient-primary @endif" href="{{ route('silders.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">dataset</i>
                    </div>
                    <span class="nav-link-text ms-1">Slider</span>
                </a>
            </li>
            @endhasPermission

            @hasPermission('coupons.index')
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('coupons.*')) active bg-gradient-primary @endif" href="{{ route('coupons.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">add_to_drive</i>
                    </div>
                    <span class="nav-link-text ms-1">Mã giảm giá</span>
                </a>
            </li>
            @endhasPermission

            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('admin.orders.*')) active bg-gradient-primary @endif" href="{{ route('admin.orders.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">list_alt</i>
                    </div>
                    <span class="nav-link-text ms-1">Đơn hàng</span>
                </a>
            </li>

            <li class="nav-item mt-3">
                <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Trang cá nhân</h6>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white @if (isCurrentRouteInRoutes('user.*') ) active bg-gradient-primary @endif" href="{{ route('users.information', auth()->user()->id) }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Hồ sơ</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white " href="{{ route('logout') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">login</i>
                    </div>
                    <span class="nav-link-text ms-1">Đăng xuất</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
