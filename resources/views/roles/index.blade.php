@extends('layouts.main')

@section('contents')
    <h3>Vai Trò</h3>

    @hasPermission('superadmin', 'roles.create')
    <a href="{{ route('roles.create') }}" class="btn btn-success btn-sm">+ Tạo Mới</a>
    @endhasPermission

    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Quyền</th>
            <th>Hành Động</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $role)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>
                    <div class="role-name" data-role-id="{{ $role->id }}" data-toggle="modal" data-target="#permissionsModal">
                        {{ $role->name }}
                    </div>
                    <div class="role-details" id="role-details-{{ $role->id }}" style="display: none;">
                        <ul>
                            @foreach($role->permissions as $permission)
                                <li>{{ $permission->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="row">
                            <?php
                            $permissionGroups = [];
                            ?>
                        @forelse($role->permissions as $permission)
                                <?php
                                $permissionParts = explode('.', $permission->name);
                                $permissionGroup = $permissionParts[0];
                                ?>
                            @if (!in_array($permissionGroup, $permissionGroups))
                                    <?php
                                    $permissionGroups[] = $permissionGroup;
                                    ?>
                                <div class="col-12 col-md-6">
                                    <span class="badge badge-success role-group" data-role-id="{{ $role->id }}" data-role-group="{{ $permissionGroup }}">
                                        {{ $permissionGroup }}
                                    </span>
                                </div>
                            @endif
                        @empty
                            <div class="col-12">
                                <span class="badge badge-danger">Không có quyền</span>
                            </div>
                        @endforelse
                    </div>
                </td>
                <td>
                    @hasPermission('superadmin', 'roles.edit')
                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-warning btn-sm me-1">Sửa</a>
                    @endhasPermission

                    @hasPermission('superadmin', 'roles.destroy')
                    <button type="button" class="btn btn-danger btn-delete" data-id="{{ $role->id }}" data-url="{{ route('roles.destroy', $role->id) }}">
                        <i class="bi bi-trash"></i> Xóa
                    </button>
                    <form id="form-delete{{ $role->id}}" action="{{ route('roles.destroy', $role->id) }}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="permissionsModal" tabindex="-1" role="dialog" aria-labelledby="permissionsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="permissionsModalLabel"></h5>
                </div>
                <div class="modal-body" id="modalBody">

                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-close" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.role-group').click(function () {
                var roleId = $(this).data('role-id');
                var roleGroup = $(this).data('role-group');
                var permissionsHtml = '';
                $('.role-name').each(function () {
                    var currentRoleId = $(this).data('role-id');
                    if (roleId === currentRoleId) {
                        var roleDetailsHtml = $('#role-details-' + currentRoleId).html();
                        if (roleDetailsHtml.includes(roleGroup)) {
                            permissionsHtml += roleDetailsHtml;
                        }
                    }
                });
                $('#modalBody').html(permissionsHtml);
                var roleName = $('.role-name[data-role-id="' + roleId + '"]').text();
                $('#permissionsModalLabel').text(roleName);

                $('#permissionsModal').modal('show');
            });
        });
    </script>

    <div class="d-flex justify-content-center">
        {{ $roles->links() }}
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.btn-delete', function() {
                var url = $(this).data('url');
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Bạn có chắc chắn muốn xóa?',
                    text: "Hành động này không thể hoàn tác!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Xóa',
                    cancelButtonText: 'Hủy'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#form-delete${id}`).submit();
                    }
                });
            });
        });
    </script>
@endsection
