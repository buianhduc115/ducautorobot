@extends('layouts.main')

@section('contents')
    <h1 style="text-align:center">Chỉnh Sửa Vai Trò</h1>
    <div class="container-fluid px-2 px-md-4">
        <div class="card card-body mx-3 mx-md-4 mt-n6">
            <div class="row gx-4 mb-2">
                <div class="col-auto my-auto">
                    <div class="h-100">
                        <h5 class="mb-1">
                            {{ auth()->user()->name }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card card-plain h-100">
                <form method='POST' action='{{ route('roles.update', $role->id ) }}'>
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="mb-3 col-md-6">
                            <label class="form-label">Tên</label>
                            <input type="text" name="name" class="form-control border border-2 p-2" value='{{ $role->name }}'>
                            @error('name')
                            <p class='text-danger inputerror'>{{ $message }} </p>
                            @enderror
                        </div>

                        <div class="mb-3 col-md-12">
                            <label class="form-label">Quyền</label>
                            @section('permissions-section')
                                    <?php
                                    $groupedPermissions = $permissions->groupBy(function ($permission) {
                                        return explode('.', $permission->name)[0];
                                    });
                                    $columnCounter = 0;
                                    ?>
                                <div class="row">
                                    @foreach($groupedPermissions as $group => $groupedPermission)
                                        <div class="col-md-4 mb-2">
                                            <strong>{{ $group }}</strong>
                                            @foreach($groupedPermission as $permission)
                                                <div class="custom-control custom-checkbox">
                                                    <input name="permissionIds[]" class="custom-control-input" type="checkbox"
                                                           id="{{ $permission->id }}" value="{{ $permission->id }}"
                                                           @if($role->permissions->contains('id', $permission->id))
                                                               checked
                                                        @endif
                                                    >
                                                    <label for="{{ $permission->id }}"
                                                           class="custom-control-label">{{ $permission->name }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                            <?php
                                            $columnCounter++;
                                            if ($columnCounter % 3 == 0) {
                                                echo '</div><div class="row">';
                                            }
                                            ?>
                                    @endforeach
                                </div>
                            @show
                        </div>
                    </div>
                    <button type="submit" class="btn bg-gradient-dark">Gửi</button>
                </form>
            </div>
        </div>
    </div>
@endsection
