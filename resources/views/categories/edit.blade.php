@extends('layouts.main')

@section('contents')
    <div class="card-header">Cập Nhật</div>
    <div class="container-fluid px-2 px-md-4">
        <div class="card card-plain h-100">
            <form method='POST' action='{{ route('categories.update', $category->id ) }}'>
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="mb-3 col-md-6">
                        <label class="form-label">Tên</label>
                        <input type="text" name="name" class="form-control border border-2 p-2" value='{{ $category->name }}'>
                        @error('name')
                        <p class='text-danger inputerror'>{{ $message }} </p>
                        @enderror
                    </div>
                    <div class="mb-3 col-md-6">
                        <label class="form-label">Danh Mục Cha</label>
                        <select id="my-select" name="parent_id" class="form-control border border-2 p-2" onchange="updateSelectedValue()">
                            <option value="">Chọn Danh Mục Cha...</option>
                            @foreach ($categories as $cate)
                                <option value="{{ $cate->id }}" @if ($category->parent_id == $cate->id) selected @endif>
                                    {{ $cate->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn bg-gradient-dark">Gửi</button>
            </form>
        </div>
    </div>
@endsection
