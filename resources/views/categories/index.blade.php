@extends('layouts.main')

@section('contents')
    <h3>Danh Mục</h3>
    @hasPermission('superadmin', 'categories.create')
    <a href="{{ route('categories.create') }}" class="btn btn-success btn-sm">+ Tạo Mới</a>
    @endhasPermission

    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th># STT</th>
            <th>ID</th>
            <th>Tên</th>
            <th>ID Cha</th>
            <th>Hành Động</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $index => $category)
            <tr>
                <td>{{ $index + 1 }}</td>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td class="align-middle text-center text-sm">
                    @if($category->parent)
                        <span class="badge badge-success">
                            {{ $category->parent->name }}
                        </span>
                    @else
                        Không Có
                    @endif
                </td>
                <td>
                    @hasPermission('superadmin', 'categories.edit')
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm me-1">Sửa</a>
                    @endhasPermission

                    @hasPermission('superadmin', 'categories.destroy')
                    <button type="button" class="btn btn-danger btn-delete" data-id="{{ $category->id }}" data-url="{{ route('categories.destroy', $category->id) }}">
                        <i class="bi bi-trash"></i> Xóa
                    </button>
                    <form id="form-delete{{ $category->id }}" action="{{ route('categories.destroy', $category->id) }}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $categories->links() }}
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.btn-delete', function() {
                var url = $(this).data('url');
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Bạn có chắc chắn muốn xóa?',
                    text: "Hành động này không thể hoàn tác!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Xóa',
                    cancelButtonText: 'Hủy'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#form-delete${id}`).submit();
                    }
                });
            });
        });
    </script>
@endsection
