@extends('layouts.main')

@section('contents')
    <div class="container-fluid px-2 px-md-4">
        <div class="page-header min-height-300 border-radius-xl mt-4"
             style="background-image: url('https://images.unsplash.com/photo-1531512073830-ba890ca4eba2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1920&q=80');">
            <span class="mask  bg-gradient-primary  opacity-6"></span>
        </div>
        <div class="card card-body mx-3 mx-md-4 mt-n6">
            <div class="row gx-4 mb-2">
                <div class="col-auto my-auto">
                    <div class="h-100">
                        <h5 class="mb-1">
                            {{ auth()->user()->name }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card card-plain h-100">
                <form method='POST' action='{{ route('categories.store') }}'>
                    @csrf
                    <div class="row">
                        <div class="mb-3 col-md-6">
                            <label class="form-label">Tên</label>
                            <input type="text" name="name" class="form-control border border-2 p-2" value='{{ old('name') }}'>
                            @error('name')
                            <p class='text-danger inputerror'>{{ $message }} </p>
                            @enderror
                        </div>
                        <div class="mb-3 col-md-6">
                            <label class="form-label">Danh Mục Cha</label>
                            <select id="my-select" name="parent_id" class="form-control border border-2 p-2" onchange="updateSelectedValue()">
                                <option value="">Chọn Danh Mục Cha...</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id}}">
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn bg-gradient-dark">Gửi</button>
                </form>
            </div>
        </div>
    </div>
@endsection
