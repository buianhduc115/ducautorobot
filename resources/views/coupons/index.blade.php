@extends('layouts.main')

@section('contents')
    <h3 class="mb-4">Phiếu giảm giá</h3>

    <div class="mb-3 text-right">
        @hasPermission('coupons.create')
        <a href="{{ route('coupons.create') }}" class="btn btn-success btn-sm">+ Tạo mới</a>
        @endhasPermission
    </div>

    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('coupons.index') }}" method="get" class="form-inline mt-3">
                    @csrf

                    <div class="form-group mr-3">
                        <label for="name" class="sr-only">Tìm kiếm theo tên:</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Tìm kiếm theo tên" value="{{ request('name') }}" />
                    </div>

                    <button type="submit" class="btn btn-success">Tìm kiếm</button>
                    <a href="{{ route('coupons.index') }}" class="btn btn-secondary ml-2">Xóa bộ lọc</a>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Loại</th>
            <th>Giá trị</th>
            <th>Hạn sử dụng</th>
            <th>Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($coupons as $item)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $item->name }}</td>

                <td>{{ $item->type }}</td>
                <td>{{ $item->value }}</td>
                <td>{{ $item->expery_date }}</td>
                <td>
                    <div class="btn-group">
                        @hasPermission('coupons.edit')
                        <a href="{{ route('coupons.edit', $item->id) }}" class="btn btn-warning">
                            <i class="bi bi-pencil"></i> Sửa
                        </a>
                        @endhasPermission

                        @hasPermission('coupons.destroy')
                        <button type="button" class="btn btn-danger btn-delete" data-id="{{ $item->id }}" data-url="{{ route('coupons.destroy', $item->id) }}">
                            <i class="bi bi-trash"></i> Xóa
                        </button>
                        <form id="form-delete{{ $item->id }}" action="{{ route('coupons.destroy', $item->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                        @endhasPermission
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $coupons->links() }}
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.btn-delete', function() {
                var url = $(this).data('url');
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Bạn có chắc chắn muốn xóa?',
                    text: "Hành động này không thể hoàn tác!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Xóa',
                    cancelButtonText: 'Hủy'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#form-delete${id}`).submit();
                    }
                });
            });
        });
    </script>
@endsection
