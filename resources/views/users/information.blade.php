@extends('layouts.main')

@section('contents')
    <h3>Hồ sơ cá nhân</h3>
    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Ngày sinh</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ</th>
            <th>Vai trò</th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">{{ $user->id }}</th>
            <td>{{ $user->name }}</td>
            <td>{{ $user->birthday }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td title="{{ $user->address }}">{{ Str::limit($user->address, 30) }}</td>
            <td>
                @if(Auth::user()->roles)
                    @foreach(Auth::user()->roles as $role)
                        <span class="badge badge-sm bg-gradient-success">
                            {{ $role->name }}
                        </span>
                    @endforeach
                @endif
            </td>
            <td>
                <div class="d-flex justify-content-start">
                    <a href="{{ route('users.edit', auth()->user()->id) }}" class="btn btn-warning btn-sm me-1">Chỉnh sửa</a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
@endsection
