@extends('layouts.main')

@section('contents')
    <h3 class="mb-4">Hồ sơ</h3>

    <div class="mb-3 text-right">
        @hasPermission('superadmin', 'users.create')
        <a href="{{ route('users.create') }}" class="btn btn-success btn-sm"><i class="bi bi-plus-lg"></i> Tạo mới</a>
        @endhasPermission
    </div>

    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('users.index') }}" method="get" class="form-inline mt-3">
                    @csrf

                    <div class="form-group mr-3">
                        <label for="name" class="sr-only">Tìm kiếm theo tên:</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Tìm kiếm theo tên" value="{{ request('name') }}" />
                    </div>

                    <div class="form-group mr-3">
                        <select class="form-control" name="role" id="role">
                            <option value="">Chọn vai trò...</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}" {{ request('role') == $role->id ? 'selected' : '' }}>
                                    {{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success"><i class="bi bi-search"></i> Tìm kiếm</button>
                    <a href="{{ route('users.index') }}" class="btn btn-secondary ml-2"><i class="bi bi-x-lg"></i> Xóa bỏ</a>
                </form>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Email</th>
            <th>Vai trò</th>
            <th>Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if ($user->roles->isNotEmpty())
                        @foreach ($user->roles as $role)
                            <span class="badge badge-success">{{ $role->name }}</span>
                        @endforeach
                    @else
                        Không có vai trò
                    @endif
                </td>
                <td>
                    <div class="btn-group" role="group">
                        <a href="{{ route('users.show', $user->id) }}" class="btn btn-primary">
                            <i class="bi bi-eye"></i> Xem
                        </a>

                        @hasPermission('superadmin', 'users.edit')
                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning">
                            <i class="bi bi-pencil"></i> Sửa
                        </a>
                        @endhasPermission

                        @hasPermission('superadmin', 'users.destroy')
                        <button type="button" class="btn btn-danger btn-delete" data-id="{{ $user->id }}" data-url="{{ route('users.destroy', $user->id) }}">
                            <i class="bi bi-trash"></i> Xóa
                        </button>
                        <form id="form-delete{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                        @endhasPermission
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $users->links() }}
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.btn-delete', function() {
                var url = $(this).data('url');
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Bạn có chắc chắn muốn xóa?',
                    text: "Hành động này không thể hoàn tác!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Xóa',
                    cancelButtonText: 'Hủy'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#form-delete${id}`).submit();
                    }
                });
            });
        });
    </script>
@endsection
