@extends('layouts.main')

@section('contents')
    <h3>Thông tin chi tiết người dùng</h3>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Họ và tên</th>
            <th>Ngày sinh</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ</th>
            <th>Vai trò</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">{{ $user->id }}</th>
            <td>{{ $user->name }}</td>
            <td>{{ $user->birthday }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td title="{{ $user->address }}">{{ Str::limit($user->address, 30) }}</td>
            <td>
                @if($user->roles)
                    @foreach($user->roles as $role)
                        <span class="badge badge-sm bg-gradient-success">
                                {{ $role->name }}
                            </span><br>
                    @endforeach
                @endif
            </td>
        </tr>
        </tbody>
    </table>

    <div class="d-flex justify-content-start">
        <a href="{{ route('users.index') }}" class="btn btn-warning btn-sm me-1">Quay lại</a>
    </div>
@endsection
