@extends('layouts.main')

@section('contents')
    <div class="card">
        <div class="card-header">Cập nhật thông tin</div>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card-body">
            <form method="post" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" name="blog_id" class="form-control" value="{{ $user->id }}" readonly />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">Họ và tên:</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control"
                               value="{{ $user->name }}"  />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">Ngày sinh:</label>
                    <div class="col-sm-10">
                        <input type="text" name="birthday" class="form-control"
                               value="{{ $user->birthday }}"  />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">Email:</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control"
                               value="{{ $user->email }}"  readonly />
                    </div>
                </div>

                @hasPermission('superadmin', 'roles.edit')
                <div class="mb-3 col-md-6">
                    <label class="form-label">Vai trò</label>
                    <div class="d-flex flex-wrap">
                        @foreach($roles as $role)
                            <div class="custom-control custom-checkbox me-3">
                                <input name="roles[]" class="custom-control-input" type="checkbox" id="{{ $role->id }}" value="{{ $role->id }}"
                                       @if(in_array($role->id, $user->roles->pluck('id')->toArray()))
                                           checked
                                    @endif>
                                <label for="{{ $role->id }}" class="custom-control-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                    </div>
                    @error('roles')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                @endhasPermission

                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">Số điện thoại:</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control"
                               value="{{ $user->phone }}"  />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-label-form">Địa chỉ:</label>
                    <div class="col-sm-10">
                        <input type="text" name="address" class="form-control"
                               value="{{ $user->address }}"  />
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Cập nhật</button>

            </form>
        </div>
    </div>
@endsection
