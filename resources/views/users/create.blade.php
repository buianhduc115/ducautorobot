@extends('layouts.main')

@section('contents')
    <h1 style="text-align:center">Tạo mới</h1>
    <div class="container-fluid px-2 px-md-4">
        <div class="card card-body mx-3 mx-md-4 mt-n6">
            <div class="row gx-4 mb-2">
                <div class="col-auto my-auto">
                    <div class="h-100">
                        <h5 class="mb-1">
                            {{ auth()->user()->name }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card card-plain h-100">
                <form role="form" method="post" action="{{ route('users.store') }}" enctype="multipart/form-data">
                    @csrf

                    <!-- Tên -->
                    <div class="input-group input-group-outline mb-3">
                        <label for="name" class="form-label">Tên</label>
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required>
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Email -->
                    <div class="input-group input-group-outline mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required>
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Mật khẩu -->
                    <div class="input-group input-group-outline mb-3">
                        <label for="password" class="form-label">Mật khẩu</label>
                        <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" required>
                        @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Vai trò -->
                    <div class="mb-3 col-md-6">
                        <label class="form-label">Vai trò</label>
                        <div class="d-flex flex-wrap">
                            @foreach($roles as $role)
                                <div class="custom-control custom-radio me-3">
                                    <input name="roles" class="custom-control-input" type="radio" id="{{ $role->id }}" value="{{ $role->id }}">
                                    <label for="{{ $role->id }}" class="custom-control-label">{{ $role->name }}</label>
                                </div>
                            @endforeach
                        </div>
                        @error('roles')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Ngày sinh -->
                    <div class="d-flex align-items-center mb-3">
                        <label for="birthday" class="form-label me-3 flex-shrink-0">Ngày sinh</label>
                        <input type="date" id="birthday" name="birthday" class="form-control flex-grow-1" required>
                    </div>

                    <!-- Số điện thoại -->
                    <div class="input-group input-group-outline mb-3">
                        <label for="phone" class="form-label">Số điện thoại</label>
                        <input type="tel" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" required>
                        @error('phone')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Địa chỉ -->
                    <div class="input-group input-group-outline mb-3">
                        <label for="address" class="form-label">Địa chỉ</label>
                        <input type="text" id="address" name="address" class="form-control @error('address') is-invalid @enderror" required>
                        @error('address')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">THÊM</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
