@php use Illuminate\Support\Str; @endphp
@extends('layouts.main')

@section('contents')
    <h3 class="mb-4">Slider</h3>

    <div class="mb-3 text-right">
        @hasPermission('silders.create')
        <a href="{{ route('silders.create') }}" class="btn btn-success btn-sm">+ Tạo Mới</a>
        @endhasPermission
    </div>

    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('silders.index') }}" method="get" class="form-inline mt-3">
                    @csrf

                    <div class="form-group mr-3">
                        <label for="name" class="sr-only">Tìm kiếm theo Tên:</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Tìm kiếm theo Tên"
                               value="{{ request('name') }}"/>
                    </div>

                    <button type="submit" class="btn btn-success">Tìm Kiếm</button>
                    <a href="{{ route('silders.index') }}" class="btn btn-secondary ml-2">Xóa</a>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Mô tả</th>
            <th>Hình ảnh</th>
            <th>Thao tác</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($silders as $silder)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $silder->name }}</td>
                <td>{{ Str::limit($silder->description, 50, '...') }}</td>
                <td>
                    <img src="{{ $silder->image }}" alt=""
                         style="width:110px; height:110px; object-fit: cover;"
                         class="img-thumbnail img-circle">
                </td>
                <td>
                    <div class="btn-group">
                        @hasPermission('silders.edit')
                        <a href="{{ route('silders.edit', $silder->id) }}" class="btn btn-warning">
                            <i class="bi bi-pencil"></i> Sửa
                        </a>
                        @endhasPermission

                        @hasPermission('silders.destroy')
                        <button type="button" class="btn btn-danger btn-delete" data-id="{{ $silder->id }}" data-url="{{ route('silders.destroy', $silder->id) }}">
                            <i class="bi bi-trash"></i> Xóa
                        </button>
                        <form id="form-delete{{ $silder->id }}" action="{{ route('silders.destroy', $silder->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                        @endhasPermission
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $silders->links() }}
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on('click', '.btn-delete', function() {
                var url = $(this).data('url');
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Bạn có chắc chắn muốn xóa?',
                    text: "Hành động này không thể hoàn tác!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Xóa',
                    cancelButtonText: 'Hủy'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(`#form-delete${id}`).submit();
                    }
                });
            });
        });
    </script>
@endsection
