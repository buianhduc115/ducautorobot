@extends('layouts.main')

@section('contents')
    <div class="container-fluid px-2 px-md-4">
        <div class="card mx-auto mt-5" style="max-width: 600px;">
            <div class="card-header">
                <h1 class="text-center">Tạo mới Silder</h1>
            </div>
            <div class="card-body">
                <div class="mb-3 text-center">
                    <h5 class="mb-1">{{ auth()->user()->name }}</h5>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('silders.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="mb-3">
                        <label for="name" class="form-label">Tên</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="description" class="form-label">Mô tả</label>
                        <input type="text" id="description" name="description" class="form-control" required>
                    </div>

                    <div class="mb-3">
                        <label for="image" class="form-label">Hình ảnh</label>
                        <input type="file" id="image" name="image" class="form-control" accept="image/*">
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-lg w-100">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
