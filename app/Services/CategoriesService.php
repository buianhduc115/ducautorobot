<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\CategoriesRepository;
use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
class CategoriesService
{
    protected $categoryRepository;

    public function __construct(CategoriesRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function search(Request $request)
    {
        $data = $request->all();
        return $this->categoryRepository->search($data);
    }
    public function getParentCategories()
    {
        return $this->categoryRepository->getParentCategories();
    }
    public function create(StoreRequest $request)
    {
        $data = $request->all();

        return $this->categoryRepository->create($data);
    }

    public function findOrFail(string $id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function update(UpdateRequest $request, string $id)
    {
        $data = $request->all();
        return $this->categoryRepository->update($data, $id);
    }

    public function delete(string $id)
    {
        return $this->categoryRepository->delete($id);
    }
}
