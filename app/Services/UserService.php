<?php

namespace App\Services;

use App\Models\Role;
use App\Http\Requests\Users\StoreRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserService
{
    protected $userRepository;
    protected $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function getAllUsers()
    {
        return $this->userRepository->all();
    }

    public function searchUsers(Request $request)
    {
        $data = $request->all();
        return $this->userRepository->search($data);
    }

    public function listRoles()
    {
        return $this->roleRepository->all();
    }

    public function createUser(StoreRequest $request)
    {
        $data = $request->all();
        $user = $this->userRepository->create($data);
        $user->syncRoles($request->input('roles'));
        return $user;
    }

    public function registerUser(StoreRequest $request)
    {
        $data = $request->all();
        $user = $this->userRepository->create($data);
        $user->syncRoles([Role::where('name', 'user')->first()->id]);
        return $user;
    }

    public function getUserById($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function updateUser($request, $id)
    {
        $user = $this->userRepository->find($id);
        $roles = $request->input('roles', []);
        if (empty($roles)) {
            $roles = $user->roles->pluck('id')->toArray();
        }
        $user = $this->userRepository->update($id);
        $user->syncRoles($roles);
        return $user;
    }

    public function deleteUser($id)
    {
        $user = $this->userRepository->find($id);
        return $this->userRepository->delete($id);
    }

    public function findOrCreateUser($googleUser)
    {
        $user = $this->userRepository->findByEmail($googleUser->getEmail());
        if ($user === null) {
            $user = $this->userRepository->create([
                'name' => $googleUser->getName(),
                'email' => $googleUser->getEmail(),
                'password' => bcrypt(Str::random(16)),
                'birthday' => '01-01-1999',
                'phone' => '',
                'address' => '',
            ]);
            $user->syncRoles([Role::where('name', 'user')->first()->id]);
        }
        return $user;
    }
}
