<?php

namespace App\Services;

use App\Mail\OrderCreated;
use App\Repositories\CartRepository;
use App\Models\CartProduct;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CartService
{
    protected $cartRepository;
    protected $product;
    protected $cartProduct;
    protected $coupon;
    protected $order;

    public function __construct(CartRepository $cartRepository, CartProduct $cartProduct, Coupon $coupon, Order $order, Product $product)
    {
        $this->cartRepository = $cartRepository;
        $this->cartProduct = $cartProduct;
        $this->coupon = $coupon;
        $this->order = $order;
        $this->product = $product;
    }

    public function getOrCreateCartByUserId($userId)
    {
        return $this->cartRepository->getOrCreateByUserId($userId);
    }

    public function addToCart($userId, $productId, $quantity)
    {
        $cart = $this->getOrCreateCartByUserId($userId);
        $product = $this->product->find($productId);
        $productPrice = $product->price;
        $cartProduct = $this->cartProduct->where('cart_id', $cart->id)
            ->where('product_id', $productId)
            ->first();

        if ($cartProduct) {
            $cartProduct->product_quantity += $quantity;
            $cartProduct->save();
        } else {
            $cartProduct = $this->cartProduct->create([
                'cart_id' => $cart->id,
                'product_stock' => $product->stock,
                'product_id' => $productId,
                'product_price' => $productPrice,
                'product_quantity' => 0,
                'user_id' => $userId
            ]);
        }
        $cartProduct->product_quantity += $quantity;
        $cartProduct->save();
        return $cart;
    }

    public function removeProductInCart($id)
    {
        $cartProduct = $this->cartProduct->find($id);
        $cartProduct->delete();
        return $cartProduct->cart;
    }

    public function updateQuantityProduct(Request $request, $id)
    {
        $cartProduct = $this->cartProduct->find($id);
        $dataUpdate = $request->all();
        if ($dataUpdate['product_quantity'] < 1) {
            $cartProduct->delete();
        } else {
            $cartProduct->update($dataUpdate);
        }
        return $cartProduct->cart;
    }

    public function applyCoupon(Request $request)
    {
        $name = $request->input('coupon_code');
        $coupon = $this->coupon->firstWithExperyDate($name, auth()->user()->id);

        if ($coupon) {
            $message = 'Áp Mã giảm giá thành công!';
            Session::put('coupon_id', $coupon->id);
            Session::put('discount_amount_price', $coupon->value);
            Session::put('coupon_code', $coupon->name);
        } else {
            Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);
            $message = 'Mã giảm giá không tồn tại hoặc hết hạn!';
        }

        return redirect()->route('client.carts.index')->with(['success' => $message]);
    }

    public function checkout()
    {
        return $this->cartRepository->firtOrCreateBy(auth()->user()->id);
    }

    public function processCheckout(Request $request)
    {
        $cart = $this->cartRepository->firtOrCreateBy(auth()->user()->id);
        $cartProducts = $cart->products;
        foreach ($cartProducts as $cartProduct) {
            $product = $cartProduct->product;
            $quantity = $cartProduct->product_quantity;

            if ($product->stock < $quantity) {
                return [
                    'success' => false,
                    'message' => 'Không đủ hàng cho sản phẩm: ' . $product->name
                ];
            }
        }
        $couponID = Session::get('coupon_id');
        if ($couponID) {
            $coupon = $this->coupon->find($couponID);
            if ($coupon) {
                $coupon->users()->attach(auth()->user()->id, ['value' => $coupon->value]);
            }
        }
        $dataCreate = $request->all();
        $dataCreate['user_id'] = auth()->user()->id;
        $dataCreate['status'] = 'pending';
        $dataCreate['vnp_TxnRef'] = null;
        $order = $this->order->create($dataCreate);
        Mail::to($request->input('customer_email'))->send(new OrderCreated($order));

        foreach ($cartProducts as $cartProduct) {
            $product = $cartProduct->product;
            $quantity = $cartProduct->product_quantity;
            $product->reduceStock($quantity);
            $cartProduct->delete();
        }


        Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);

        return [
            'success' => true,
            'order' => $order
        ];
    }

}
