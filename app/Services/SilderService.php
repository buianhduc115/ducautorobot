<?php

namespace App\Services;

use App\Http\Requests\Silders\StoreRequest;
use App\Http\Requests\Silders\UpdateRequest;
use App\Repositories\SilderRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;

class SilderService
{
    use HandleImage;
    public SilderRepository $silderRepository;
    public string $pathSilder = 'silders';

    public function __construct(SilderRepository $silderRepository)
    {
        $this->silderRepository = $silderRepository;
        $this->setPath($this->pathSilder);
    }
    public function searchSilders(Request $request)
    {
        $data = $request->all();
        return $this->silderRepository->search($data);
    }

    public function getLatestSilders()
    {
        return $this->silderRepository->getLatestSilders();
    }

    public function createsilder(StoreRequest $request)
    {
        $data = $request->all();
        $data['image'] = $this->saveImage($request->image);
        $silder = $this->silderRepository->create($data);
        return $silder;
    }
    public function getSilderById($id)
    {
        return $this->silderRepository->findOrFail($id);
    }
    public function updateSilder(UpdateRequest $request, $id)
    {
        $silder = $this->silderRepository->find($id);
        $dataSilder['image'] = $this->updateImage($request->image, $silder->getRawOriginal('image'));
        $silder = $this->silderRepository->update( $dataSilder, $id);
        return $silder;
    }

    public function delete($id)
    {
        $silder = $this->silderRepository->find($id);
        $this->deleteImage($silder->image);
        return $this->silderRepository->delete($id);
    }
}
