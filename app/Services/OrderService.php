<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use Illuminate\Http\Request;


class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getOrdersByUserId($userId)
    {
        return $this->orderRepository->getOrdersByUserId($userId);
    }

    public function cancelOrder($id)
    {
        return $this->orderRepository->updateOrderStatus($id, 'cancel');
    }

    public function updateOrderStatus($id, $status)
    {
        return $this->orderRepository->updateOrderStatus($id, $status);
    }
}
