<?php

namespace App\Services;

use App\Repositories\CategoriesRepository;
use App\Traits\HandleImage;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;

class ProductService
{
    use HandleImage;
    public ProductRepository $productRepository;
    public CategoriesRepository $categoryRepository;
    public string $pathProduct = 'products';

    public function __construct(ProductRepository $productRepository, CategoriesRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->setPath($this->pathProduct);
    }

    public function search(Request $request = null)
    {
        $data = $request->all();
        return $this->productRepository->search($data);
    }

    public function searchByNameOrDescription($searchTerm)
    {
        return $this->productRepository->searchByName($searchTerm);
    }

    public function getLatestProducts()
    {
        return $this->productRepository->getLatestProducts();
    }

    public function getProductsByCategory($id, $filters = [])
    {
        return $this->productRepository->getProductsByCategory($id, $filters);
    }

    public function findOrFail(string $id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function create(StoreRequest $request)
    {
        $data = $request->all();
        $data['image'] = $this->saveImage($request->image);
        $product = $this->productRepository->create($data);
        $categories = $request->input('categories');
        if ($categories !== null && isset($categories[0])) {
            $product->syncCategories($categories);
        }
        return $product;
    }


    public function update(Request $request, $id)
    {
        $product = $this->productRepository->find($id);
        $dataProduct = $request->all();
        $dataProduct['image'] = $this->updateImage($request->image, $product->getRawOriginal('image'));
        $product = $this->productRepository->update($dataProduct, $id);
        $categoryIds = $request->input('categories');
        if ($categoryIds !== null && isset($categoryIds[0])) {
            $product->syncCategories($categoryIds);
        }
        return $product;
    }

    public function delete($id): bool
    {
        $product = $this->productRepository->find($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }
}
