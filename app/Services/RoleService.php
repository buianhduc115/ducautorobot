<?php

namespace App\Services;

use App\Http\Requests\Roles\StoreRequest;
use App\Http\Requests\Roles\UpdateRequest;
use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class RoleService
{
    protected $roleRepository;
    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function getAllRoles()
    {
        return $this->roleRepository->all();
    }

    public function searchRoles(Request $request)
    {
        $data = $request->all();
        return $this->roleRepository->search($data);
    }

    public function listPermissions()
    {
        return $this->permissionRepository->all();
    }

    public function getRoleById($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function createRole(StoreRequest $request)
    {
        $data = $request->all();
        $role = $this->roleRepository->create($data);
        $role->syncPermissions($request->input('permissionIds'));

        return $role;
    }

    public function updateRole(UpdateRequest $request, $id)
    {
        $data = $request->all();
        $role = $this->roleRepository->update($data, $id);
        $permissionIds = $request->input('permissionIds');
        $role->syncPermissions($permissionIds);

        return $role;
    }

    public function deleteRole($id)
    {
        return $this->roleRepository->delete($id);
    }
}
