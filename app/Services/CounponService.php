<?php
namespace App\Services;

use App\Http\Requests\Coupons\StoreRequest;
use App\Http\Requests\Coupons\UpdateRequest;
use App\Repositories\CounponRepository;
use Illuminate\Http\Request;

class CounponService
{
    public CounponRepository $counponRepository;
    public function __construct(CounponRepository $counponRepository)
    {
        $this->counponRepository = $counponRepository;
    }
    public function searchCounpon(Request $request)
    {
        $data = $request->all();
        return $this->counponRepository->search($data);
    }

    public function createCounpon(StoreRequest $request)
    {
        $data = $request->all();
        $counpon = $this->counponRepository->create($data);
        return $counpon;
    }
    public function getSilderById($id)
    {
        return $this->counponRepository->findOrFail($id);
    }
    public function updateCounpon(UpdateRequest $request, $id)
    {
        $data = $request->all();
        $updatedCounpon = $this->counponRepository->update($data, $id);
        return $updatedCounpon;
    }

    public function delete($id)
    {
        return $this->counponRepository->delete($id);
    }
}

