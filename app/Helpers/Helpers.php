<?php
if (!function_exists('isCurrentRouteInRoutes')) {
    /**
     * Check current route in listed routes
     * String sample input: 'user.classes.*' | 'user.dashboard'
     *
     * @param array|string $routes
     * @return bool
     */
    function isCurrentRouteInRoutes(array|string $routes): bool
    {
        $currentRoutes = request()->route()->getName();
        if (is_array($routes)) {
            return in_array($currentRoutes, $routes);
        }

        preg_match('/' . $routes . '/', $currentRoutes, $matches);

        return (bool)count($matches);
    }
}
