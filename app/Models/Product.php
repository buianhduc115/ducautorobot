<?php

namespace App\Models;
use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Product extends Model
{
    use HasFactory, HandleImage;
    protected $table = 'products';
    const PATH = 'storage/images/products/';
    protected $fillable = [
        'name',
        'image',
        'price',
        'stock',
        'descriptions',
        'age_range',
    ];
    protected function image(): Attribute
    {
        $this->setPath(self::PATH);
        return Attribute::make(
            get: fn($image) => ($this->getImageFullPath($image))
        );
    }

    public function categories()
    {
        return $this->belongsToMany(Categorie::class, 'products_categories', 'product_id', 'categorie_id');
    }

    public function syncCategories($categoriesIds): array
    {
        return $this->categories()->sync($categoriesIds);
    }

    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }

    public function scopeByCategory(Builder $query, $categories): Builder
    {
        return $categories ? $query->whereHas('categories', function ($query) use ($categories) {
            $query->where('categorie_id', $categories);
        }) : $query;
    }

    public function reduceStock($quantity)
    {
        $this->stock -= $quantity;
        $this->save();
    }

    public function scopeByPriceRange(Builder $query, $priceRange): Builder
    {
        if (is_array($priceRange)) {
            if (in_array('price-1', $priceRange)) {
                $query->whereBetween('price', [0, 100000]);
            } elseif (in_array('price-2', $priceRange)) {
                $query->whereBetween('price', [100000, 200000]);
            } elseif (in_array('price-3', $priceRange)) {
                $query->whereBetween('price', [200000, 300000]);
            } elseif (in_array('price-4', $priceRange)) {
                $query->whereBetween('price', [300000, 400000]);
            } elseif (in_array('price-5', $priceRange)) {
                $query->where('price', '>=', 400000);
            }
        }

        return $query;
    }

    public function scopeByAgeRange($query, $ageRange)
    {
        if ($ageRange) {
            return $query->where('age_range', $ageRange);
        }
        return $query;
    }

}
