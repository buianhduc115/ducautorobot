<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    const ROLE_SUPER_ADMIN = 'superadmin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'gender',
        'birthday',
        'email',
        'phone',
        'password',
        'address',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
    public function setPasswordAttribute(string $password)
    {
        return  $this->attributes['password'] = bcrypt($password);
    }
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'roles_users');
    }
    public function syncRoles($roles)
    {
        return  $this->roles()->sync($roles);
    }
    public function hasRole($roleName): bool
    {
        return $this->roles()->where('name', $roleName)->exists();
    }

    public function hasPermission($permissionName): bool
    {
        if ($this->isSuperAdmin()) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }
        return false;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(self::ROLE_SUPER_ADMIN);
    }
    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }

    public function scopeWithRole(Builder $query, string $role = null)
    {
        return $role ? $query->whereHas('roles', fn ($innerQuery) => $innerQuery->where('role_id', $role)) : $query;
    }
}
