<?php

namespace App\Models;

use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Silder extends Model
{
    use HasFactory, HandleImage;
    const PATH = 'storage/images/silders';

    protected $table = 'silders';
    protected $fillable = [
        'name',
        'description',
        'image'
    ];
    protected function image(): Attribute
    {
        $this->setPath(self::PATH);
        return Attribute::make(
            get: fn($image) => ($this->getImageFullPath($image))
        );
    }

    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }
}
