<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'parent_id'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_categories');
    }

    public function parent()
    {
        return $this->belongsTo(Categorie::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Categorie::class, 'parent_id');
    }

    public function scopeWithNameLikeCategory(Builder $query, string $name = null)
    {
        return $name ?
            $query->where('name', 'like', '%' . $name . '%') :
            $query;
    }

    public function scopeWithParentId(Builder $query, string $id = null)
    {
        return $id ? $query->where('parent_id', $id) : $query;
    }
}
