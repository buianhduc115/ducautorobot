<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $fillable = [
        'name'
    ];

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'roles_users');
    }
    public function syncPermissions($permissionIds): array
    {
        return $this->permissions()->sync($permissionIds);
    }
    public function syncUsers($userIds): array
    {
        return $this->users()->sync($userIds);
    }
    public function hasPermission($permissionName): bool
    {
        return $this->permissions->pluck('name')->contains($permissionName);
    }
    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }
}
