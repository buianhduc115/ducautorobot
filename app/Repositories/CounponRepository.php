<?php

namespace App\Repositories;

use App\Models\Coupon;
use App\Repositories\BaseRepository;

class CounponRepository extends BaseRepository
{
    public function model()
    {
        return Coupon::class;
    }
    public function search(array $data)
    {
        $query = $this->model
            ->withName($data['name'] ?? null)
            ->paginate(10);
        return $query;
    }
}
