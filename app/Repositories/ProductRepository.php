<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model(): string
    {
        return Product::class;
    }

    public function search($data)
    {
        return $this->model->withName($data['name'])
            ->ByCategory($data['category'])
            ->latest('id')
            ->paginate(10);
    }
    public function getLatestProducts()
    {
        return $this->model->paginate(8);
    }
    public function getProductsByCategory($id, $filters = [])
    {
        return $this->model->with('categories')
            ->ByCategory($id)
            ->withName($filters['name'] ?? null)
            ->ByPriceRange($filters['price_range'] ?? null)
            ->ByAgeRange($filters['age_range'] ?? null)
            ->paginate(10);
    }

    public function searchByName($searchTerm)
    {
        return $this->model->where('name', 'like', "%$searchTerm%")
            ->latest('id')
            ->paginate(10);
    }
}
