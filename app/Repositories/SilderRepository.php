<?php

namespace App\Repositories;

use App\Models\Silder;
use App\Repositories\BaseRepository;

class SilderRepository extends BaseRepository
{
    public function model()
    {
        return Silder::class;
    }
    public function search(array $data)
    {
        $query = $this->model
            ->withName($data['name'] ?? null)
            ->paginate(10);
        return $query;
    }
    public function getLatestSilders()
    {
        return $this->model->latest()->get();
    }
}
