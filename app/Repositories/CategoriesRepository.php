<?php

namespace App\Repositories;

use App\Models\Categorie;
use App\Repositories\BaseRepository;

class CategoriesRepository extends BaseRepository
{
    public function model()
    {
        return Categorie::class;
    }
    public function search(array $data)
    {
        return $this->model->withNameLikeCategory($data['name'] ?? null)
            ->withParentId($data['parent_id'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
    public function getParentCategories()
    {
        return $this->model->where('parent_id', null)->get();
    }
}
