<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;
class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }
    public function search($search = null)
    {
        return $this->model->withName($data['name'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
}
