<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Repositories\BaseRepository;

class CartRepository extends BaseRepository
{
    public function model()
    {
        return Cart::class;
    }
    public function getOrCreateByUserId($userId)
    {
        return $this->model->firstOrCreate(['user_id' => $userId]);
    }
    public function getBy($userId)
    {
        return Cart::whereUserId($userId)->first();
    }
    public function firtOrCreateBy($userId)
    {
        $cart = $this->getBy($userId);

        if (!$cart) {
            $cart = $this->cart->create(['user_id' => $userId]);
        }
        return $cart;
    }
}
