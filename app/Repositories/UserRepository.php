<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use App\Repositories\BaseRepository;
class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }
    public function search(array $data)
    {
        $query = $this->model
            ->withName($data['name'] ?? null)
            ->withRole($data['role'] ?? null)
            ->latest('id')
            ->paginate(10);
        return $query;
    }
    public function createUserWithRole(string $role)
    {
        $user = User::factory()->create();
        if ($role == 'superadmin') {
            $superadminRole = Role::where('name', 'superadmin')->first();
            if (!$superadminRole) {
                $superadminRole = Role::factory()->create(['name' => 'superadmin']);
            }
            $user->roles()->attach($superadminRole->id);
        } elseif ($role == 'admin') {
            $adminRole = Role::where('name', 'admin')->first();
            if (!$adminRole) {
                $adminRole = Role::factory()->create(['name' => 'admin']);
            }
            $user->roles()->attach($adminRole->id);
        }
        return $user;
    }

    public function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}
