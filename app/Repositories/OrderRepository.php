<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function model()
    {
        return Order::class;
    }
    public function getOrdersByUserId($userId)
    {
        return $this->model->whereUserId($userId)->latest('id')->paginate(10);
    }

    public function findOrderById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function updateOrderStatus($id, $status)
    {
        $order = $this->model->findOrFail($id);
        $order->update(['status' => $status]);
        return $order;
    }
}
