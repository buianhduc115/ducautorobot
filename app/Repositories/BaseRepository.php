<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel(): void
    {
        $this->model = app()->make($this->model());
    }

    public function all()
    {
        return $this->model->all();
    }

    public function latest($column)
    {
        return $this->model->latest($column);
    }
    public function paginate(int $perPage = 10)
    {
        return $this->model->paginate($perPage);
    }

    public function find(int $id,array $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;
    }

    public function delete(int $id)
    {
        $model = $this->model->findOrFail($id);
        return $model->delete();
    }
}
