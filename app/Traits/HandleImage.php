<?php

namespace App\Traits;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected $qualityImg = 80;
    protected $path;
    protected $disk = 'image';

    public function verify($image): bool
    {
        return !empty($image);
    }

    public function saveImage($image): string|null
    {
        if ($this->verify($image)) {
            $extension = $image->getClientOriginalExtension();
            $image = Image::make($image)
                ->encode($extension, $this->qualityImg);
            $name = time() . '.' . $extension;
            $pathImage = $this->getImagePath($name);
            Storage::disk($this->disk)->put($pathImage, $image);
            return $name;
        }
        return null;
    }

    public function setPath(string $path): void
    {
        $this->path = $path . '/';
    }

    public function updateImage($image, $currentImage): string|null
    {
        if ($this->verify($image)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($image);
        }
        return $currentImage;
    }

    public function deleteImage($name): void
    {
        if ($this->existsImage($name) && !empty($name)) {
            Storage::disk($this->disk)->delete($this->getImagePath($name));
        }
    }

    public function existsImage($name): bool
    {
        return Storage::disk($this->disk)->exists($this->getImagePath($name));
    }

    public function getImagePath($name): string
    {
        return $this->path . $name;
    }

    public function getImageFullPath($name): string
    {
        if ($this->verify($name)) {
            return url($this->getImagePath($name));
        }
        return asset('customs/images/default.jpg');
    }

}
