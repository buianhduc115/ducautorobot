<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\StoreRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->searchUsers($request);
        $roles = $this->roleService->getAllRoles();
        return view('users.index', compact('users', 'roles'));
    }

    public function show($id)
    {
        $user = $this->userService->getUserById($id);
        return view('users.show', compact('user'));
    }

    public function create()
    {
        $roles = $this->userService->listRoles();
        return view('users.create', compact('roles'));
    }

    public function store(StoreRequest $request)
    {
        $this->userService->createUser($request);

        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = $this->userService->getUserById($id);
        $roles = $this->userService->listRoles();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $loggedInUserId = Auth::id();
        if ($id == $loggedInUserId) {
            $this->userService->updateUser($request, $id);
            return redirect()->route('users.information', $id)->with('success', 'Cập nhật người dùng thành công.');
        } else {
            $this->userService->updateUser($request, $id);
            return redirect()->route('users.show', $id)->with('success', 'Cập nhật người dùng thành công.');
        }
    }

    public function destroy($id)
    {
        $this->userService->deleteUser($id);
        return redirect()->route('users.index')->with('success', 'Xoá người dùng thành công.');
    }

    public function information($id)
    {
        $user = $this->userService->getUserById($id);
        return view('users.information', compact('user'));
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback(UserService $userService)
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $user = $userService->findOrCreateUser($googleUser);
            Auth::login($user);
            $user = Auth::user();
            return redirect()->route('users.information', ['id' => $user->id])->with('success', 'Đăng nhập thành công!');
        } catch (\Exception $e) {
            return redirect()->route('login')->withErrors(['errorLogin' => 'Có sự cố khi đăng nhập bằng Google.']);
        }
    }
}
