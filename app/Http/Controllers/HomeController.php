<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Product;
use App\Models\Silder;
use App\Services\CategoriesService;
use App\Services\ProductService;
use App\Services\SilderService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $silderService;
    protected $categoriesService;
    protected $productsService;


    public function __construct( SilderService $silderService, CategoriesService $categoriesService, ProductService $productsService)
    {
        $this->silderService = $silderService;
        $this->categoriesService = $categoriesService;
        $this->productsService = $productsService;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoriesService->getParentCategories();
        $products = $this->productsService->getLatestProducts();
        return view('fontend.index', compact('silders', 'categories','products'));
    }
    public function search(Request $request)
    {
        $searchTerm = $request->input('search');
        $products = $this->productsService->searchByNameOrDescription($searchTerm);
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoriesService->getParentCategories();
        return view('fontend.index', compact('silders', 'categories','products'));
    }
}
