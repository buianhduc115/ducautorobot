<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Services\CategoriesService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoriesService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->all();
        return view('products.index', compact('categories'));
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        $productsCollection = new ProductCollection($products);

        return response()->json([
            'status_code' => Response::HTTP_OK,
            'data' => $productsCollection,
        ], Response::HTTP_OK);
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'data' => $categories,
        ], Response::HTTP_OK);
    }

    public function store(StoreRequest $request)
    {
        $this->productService->create($request);
        return response()->json([
            'status_code' => Response::HTTP_CREATED,
            'message' => 'Tạo mới sản phẩm thành công.',
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $product = $this->productService->findOrFail($id);
        $productResource = new ProductResource($product);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'data' => $productResource,
        ], Response::HTTP_OK);
    }

    public function edit($id)
    {
        $product = $this->productService->findOrFail($id);
        $productResource = new ProductResource($product);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'data' => $productResource,
        ], Response::HTTP_OK);
    }

    public function update(UpdateRequest $request, $id)
    {
        $this->productService->update($request, $id);
        return response()->json([
            'status_code' => Response::HTTP_NO_CONTENT,
            'message' => 'Cập nhật sản phẩm thành công.',
        ], Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $this->productService->delete($id);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Xóa sản phẩm thành công.',
        ], Response::HTTP_NO_CONTENT);
    }
}
