<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\CreateOrderRequest;
use App\Mail\OrderCreated;
use App\Repositories\CartRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Models\Order;
use Illuminate\Support\Facades\Mail;
use App\Models\Product;
use App\Models\CartProduct;

class VNPayController extends Controller
{
    protected $cartRepository;
    public function __construct(CartRepository $cartRepository,)
    {
        $this->cartRepository = $cartRepository;
    }
    public function createPayment(CreateOrderRequest $request)
    {
        $cart = $this->cartRepository->firtOrCreateBy(auth()->user()->id);
        $cartProducts = $cart->products;
        foreach ($cartProducts as $cartProduct) {
            $product = $cartProduct->product;
            $quantity = $cartProduct->product_quantity;

            if ($product->stock < $quantity) {
                return redirect()->route('client.carts.index')->with('error', 'Không đủ hàng cho sản phẩm: ' . $product->name);
            }
        }

        $vnp_TmnCode = env('VNPAY_TMN_CODE');
        $vnp_HashSecret = env('VNPAY_HASH_SECRET');
        $vnp_Url = env('VNPAY_URL');
        $vnp_Returnurl = route('vnpay.return');
        $vnp_TxnRef = date("YmdHis");
        $vnp_OrderInfo = 'Payment for order';
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request->total * 100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = '';
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }

        $request->session()->put('order_info', [
            'customer_name' => $request->customer_name,
            'customer_email' => $request->customer_email,
            'customer_phone' => $request->customer_phone,
            'customer_address' => $request->customer_address,
            'note' => $request->note,
            'total' => $request->total,
            'vnp_TxnRef' => $vnp_TxnRef,
        ]);

        return redirect($vnp_Url);
    }

    public function return(Request $request)
    {
        $vnp_HashSecret = env('VNPAY_HASH_SECRET');
        $vnp_SecureHash = $_GET['vnp_SecureHash'];
        $inputData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }
        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);

        if ($secureHash == $vnp_SecureHash) {
            if ($request->vnp_ResponseCode == '00') {
                $orderInfo = $request->session()->get('order_info');

                $order = Order::create([
                    'user_id' => auth()->user()->id,
                    'customer_name' => $orderInfo['customer_name'],
                    'customer_email' => $orderInfo['customer_email'],
                    'customer_phone' => $orderInfo['customer_phone'],
                    'customer_address' => $orderInfo['customer_address'],
                    'note' => $orderInfo['note'],
                    'total' => $orderInfo['total'],
                    'status' => 'Accept',
                    'payment' => 'VNPay',
                    'vnp_TxnRef' => $orderInfo['vnp_TxnRef'],
                    'ship' => '35000',
                ]);

                Mail::to($orderInfo['customer_email'])->send(new OrderCreated($order));
                $cartProducts = CartProduct::where('user_id', auth()->user()->id)->get();
                foreach ($cartProducts as $cartProduct) {
                    $product = Product::find($cartProduct->product_id);
                    $product->stock -= $cartProduct->product_quantity;
                    $product->save();
                    $cartProduct->delete();
                }
                return redirect()->route('client.carts.index')->with('success', 'Đặt hàng thành công.');
            } else {
                return redirect()->route('client.carts.index')->with('error', 'Thanh toán thật bại.');
            }
        } else {
            return redirect()->route('client.carts.index')->with('error', 'Thanh toán thật bại.');
        }
    }
}
