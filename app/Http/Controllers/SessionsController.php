<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authenticate\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('auths.signin');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->hasRole('admin') || $user->hasRole('superadmin')) {
                return redirect()->route('dashboard')->with('success', 'Đăng nhập thành công!');
            } elseif ($user->hasRole('user')) {
                return redirect()->route('users.information', ['id' => $user->id])->with('success', 'Đăng nhập thành công!');
            }
        }
        return redirect()->route('login')->withErrors([
            'errorLogin' => 'Email hoặc mật khẩu bạn nhập không đúng.',
        ]);
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {
        Auth::logout();
        return redirect()->route('login')->with('status', 'Tài khoản đã được đăng xuất.');
    }
}
