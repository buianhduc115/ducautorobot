<?php

namespace App\Http\Controllers;

use App\Models\Silder;
use App\Services\SilderService;
use Illuminate\Http\Request;
use App\Http\Requests\Silders\StoreRequest;
use App\Http\Requests\Silders\UpdateRequest;
use Illuminate\Support\Facades\Auth;

class SilderController extends Controller
{
    protected $silderService;
    public function __construct(SilderService $silderService)
    {
        $this->silderService = $silderService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $silders = $this->silderService->searchSilders($request);
        return view('silders.index', compact( 'silders'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('silders.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $this->silderService->createSilder($request);
        return redirect()->route('silders.index')->with('success', 'Tạo Silder thành công.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Silder $silder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $silder = $this->silderService->getSilderById($id);
        return view('silders.edit', compact('silder'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->silderService->updateSilder($request, $id);
        return redirect()->route('silders.index')->with('success', 'Sửa Silder thành công.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $this->silderService->delete($id);
        return redirect()->route('silders.index')->with('success', 'Xoá Silder thành công.');
    }
}
