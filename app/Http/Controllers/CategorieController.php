<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
use App\Http\Resources\ProductCollection;
use App\Models\Categorie;
use App\Models\Product;
use App\Models\Silder;
use App\Services\CategoriesService;
use App\Services\ProductService;
use App\Services\SilderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class CategorieController extends Controller
{
    protected $categoryService;
    protected $productService;
    protected $silderService;

    public function __construct(CategoriesService $categoryService, ProductService $productService, SilderService $silderService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
        $this->silderService = $silderService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);
        $categoriesSearch = $this->categoryService->all();
        return view('categories.index', compact('categories', 'categoriesSearch'));
    }

    public function list(Request $request, $id)
    {
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoryService->getParentCategories();
        $products = $this->productService->getProductsByCategory($id, $request->all());
        $categoryId = $id;
        return view('products.category.list',compact('categories','products','silders','categoryId'));
    }

    public function showDetailProduct($id)
    {
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoryService->getParentCategories();
        $product = $this->productService->findOrFail($id);
        return view('fontend.products.detail', compact('product','categories','silders'));
    }


    public function create()
    {
        $categories = $this->categoryService->all();
        return view('categories.create', compact('categories'));
    }

    public function store(StoreRequest $request)
    {
        $this->categoryService->create($request);

        return redirect()->route('categories.index')->with('success', 'Tạo danh mục thành công.');
    }

    public function show(string $id)
    {
        $category = $this->categoryService->findOrFail($id);

        return view('categories.show', compact('category'));
    }

    public function edit(string $id)
    {
        $categories = $this->categoryService->all();
        $category = $this->categoryService->findOrFail($id);

        return view('categories.edit', compact('category', 'categories'));
    }

    public function update(UpdateRequest $request, string $id)
    {
        $this->categoryService->update($request, $id);

        return redirect()->route('categories.index')->with('success', 'Sửa danh mục thành công.');
    }

    public function destroy(string $id)
    {
        $this->categoryService->delete($id);

        return redirect()->route('categories.index')->with('success', 'Xoá danh mục thành công.');
    }

    public function getListCategory()
    {
        $categories = $this->categoryService->all();

        return response()->json([
            'status_code' => Response::HTTP_OK,
            'data' => $categories,
        ], Response::HTTP_OK);
    }
}
