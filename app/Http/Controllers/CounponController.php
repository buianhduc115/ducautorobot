<?php

namespace App\Http\Controllers;

use App\Services\CounponService;
use Illuminate\Http\Request;
use App\Http\Requests\Coupons\StoreRequest;
use App\Http\Requests\Coupons\UpdateRequest;
use App\Models\Product;

class CounponController extends Controller
{
    protected $counponService;
    public function __construct(CounponService $counponService)
    {
        $this->counponService = $counponService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $coupons = $this->counponService->searchCounpon($request);
        return view('coupons.index', compact( 'coupons'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::all();
        return view('coupons.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $this->counponService->createCounpon($request);
        return redirect()->route('coupons.index')->with('success', 'Tạo mã giảm giá thành công.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit( $id)
    {
        $coupon = $this->counponService->getSilderById($id);
        return view('coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request,$id)
    {
        $this->counponService->updateCounpon($request, $id);
        return redirect()->route('coupons.index')->with('success', 'Cập nhật mã giảm giá thành công.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->counponService->delete($id);
        return redirect()->route('coupons.index')->with('success', 'Xóa mã giảm giá thành công.');
    }
}
