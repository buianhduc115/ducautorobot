<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\CreateOrderRequest;
use App\Http\Resources\CartResource;
use App\Models\CartProduct;
use App\Services\CartService;
use App\Services\CategoriesService;
use App\Services\SilderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Mail\OrderCreated;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    protected $cartService;
    protected $cartProduct;
    protected $silderService;
    protected $categoriesService;

    public function __construct(CartService $cartService,CartProduct $cartProduct, SilderService $silderService, CategoriesService $categoriesService,)
    {
        $this->cartService = $cartService;
        $this->cartProduct = $cartProduct;
        $this->silderService = $silderService;
        $this->categoriesService = $categoriesService;
    }

    public function index()
    {
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoriesService->getParentCategories();
        $cart = $this->cartService->getOrCreateCartByUserId(auth()->user()->id);
        return view('fontend.carts.index', compact('cart','categories','silders'));
    }

    public function store(Request $request)
    {
        $userId = auth()->user()->id;
        $productId = $request->input('product_id');
        $quantity = $request->input('quantity', 1);
        $cart = $this->cartService->addToCart($userId, $productId, $quantity);
        return redirect()->route('client.carts.index')->with('success', 'Sản phẩm được thêm vào giỏ hàng thành công.');
    }

    public function removeProductInCart($id)
    {
        $cart = $this->cartService->removeProductInCart($id);
        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart)
        ], Response::HTTP_OK);
    }

    public function updateQuantityProduct(Request $request, $id)
    {
        $cartProduct =  $this->cartProduct->find($id);
        $dataUpdate = $request->all();
        if($dataUpdate['product_quantity'] < 1 ) {
            $cartProduct->delete();
        } else {
            $cartProduct->update($dataUpdate);
        }

        $cart =  $cartProduct->cart;

        return response()->json([
            'product_cart_id' => $id,
            'cart' => new CartResource($cart),
            'remove_product' => $dataUpdate['product_quantity'] < 1,
            'cart_product_price' => $cartProduct->total_price
        ], Response::HTTP_OK);
    }

    public function applyCoupon(Request $request)
    {
        return $this->cartService->applyCoupon($request);
    }

    public function checkout()
    {
        $cart = $this->cartService->checkout();
        return view('fontend.carts.checkout', compact('cart'));
    }

    public function processCheckout(CreateOrderRequest $request)
    {
        $result = $this->cartService->processCheckout($request);

        if ($result['success']) {
            return redirect()->route('client.orders.index')->with('success', 'Đặt hàng thành công.');
        } else {
            return redirect()->route('client.carts.index')->with('error', $result['message']);
        }
    }

}
