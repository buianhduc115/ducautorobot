<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StoreRequest;
use App\Http\Requests\Roles\UpdateRequest;
use App\Services\RoleService;
use Illuminate\Http\Request;


class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->searchRoles($request);
        $rolesSearch = $this->roleService->getAllRoles();
        return view('roles.index', compact('roles', 'rolesSearch'));
    }

    public function show($id)
    {
        $role = $this->roleService->getRoleById($id);
        return view('roles.show', compact('role'));
    }

    public function create()
    {
        $permissions = $this->roleService->listPermissions();
        return view('roles.create', compact('permissions'));
    }

    public function store(StoreRequest $request)
    {
        $role = $this->roleService->createRole($request);
        return redirect()->route('roles.index')->with('success', 'Vai trò được tạo thành công.');
    }

    public function edit($id)
    {
        $role = $this->roleService->getRoleById($id);
        $permissions = $this->roleService->listPermissions();
        return view('roles.edit', compact('role', 'permissions'));
    }

    public function update(UpdateRequest $request, $id)
    {
        $role = $this->roleService->updateRole($request, $id);

        return redirect()->route('roles.index')->with('success', 'Đã cập nhật vai trò thành công.');
    }

    public function destroy($id)
    {
        $this->roleService->deleteRole($id);
        return redirect()->route('roles.index')->with('success', 'Đã xóa vai trò thành công.');
    }
}
