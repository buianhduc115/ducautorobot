<?php

namespace App\Http\Controllers;

use App\Services\CategoriesService;
use App\Services\OrderService;
use App\Services\SilderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    protected $orderService;
    protected $silderService;
    protected $categoriesService;

    public function __construct(SilderService $silderService, CategoriesService $categoriesService,OrderService $orderService)
    {
        $this->orderService = $orderService;
        $this->silderService = $silderService;
        $this->categoriesService = $categoriesService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $silders = $this->silderService->getLatestSilders();
        $categories = $this->categoriesService->getParentCategories();
        $orders = $this->orderService->getOrdersByUserId(auth()->user()->id);
        return view('fontend.orders.index', compact('orders','categories','silders'));
    }

    public function indexAdmin()
    {
        $orders = $this->orderService->getOrdersByUserId(auth()->user()->id);
        return view('fontend.orders.admin', compact('orders'));
    }

    public function cancel($id)
    {
        $this->orderService->cancelOrder($id);
        return redirect()->route('client.orders.index')->with(['success' => 'Hủy thành công.']);
    }

    public function updateStatus(Request $request, $id)
    {
        $this->orderService->updateOrderStatus($id, $request->status);
        return response()->json([
            'success' => 'Sửa thành công.'
        ], Response::HTTP_OK);
    }
}
