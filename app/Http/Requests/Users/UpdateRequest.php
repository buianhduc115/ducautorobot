<?php

namespace App\Http\Requests\Users;

use App\Models\User;
use App\Rules\EmailRule;
use App\Rules\PhoneRule;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'birthday' => 'required',
            'email' => [
                'email',
                'max:255',
                Rule::unique(User::class)->ignore($this->id),
            ],
            'phone' => [
                'required',
                new PhoneRule(),
                Rule::unique(User::class)->ignore($this->id),
            ],
            'password' => 'nullable|string|min:6',
            'address' => 'required',
        ];
    }
}
