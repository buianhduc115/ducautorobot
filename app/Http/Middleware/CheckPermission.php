<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $permission
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission): Response
    {
        $user = Auth::user();
        if ($user->hasRole('superadmin') || $user->hasPermission($permission)){
            return $next($request);
        }
        return abort(Response::HTTP_FORBIDDEN)->withErrors([
            'warning' => 'This action is unauthorized.',
        ]);
    }
}
