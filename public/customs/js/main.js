$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

export const main = (function () {
  const DELAY_TIMEOUT = 500
  const module = {}

  module.debounce = function (func, delay = DELAY_TIMEOUT) {
    let timer
    return function () {
      clearTimeout(timer)
      timer = setTimeout(() => {
        func.apply(this, arguments)
      }, delay)
    }
  }

  module.paginateActive = function () {
    const paginate = $('.pagination .active span')
    console.log(paginate.val())
  }

  module.renderError = function (errors) {
    this.removeError()
    for (const key in errors) {
      $(`.${key}-error`).text(errors[key][0])
    }
  }

  module.removeError = function () {
    $('.error').html('')
    $('.input-image').val('')
  }

  module.sendAjax = function (url, method = 'POST', data = null) {
    const isFormData = data instanceof FormData
    const contentType = isFormData ? false : 'application/json'

    return $.ajax({
      url,
      method,
      data,
      contentType,
      processData: false,
      dataType: 'json',
      async: false
    })
  }

  module.lists = async function (url = null) {
    const form = $('#form-search')
    const link = url ?? form.attr('action')
    const data = form.serialize()
    await this.getLists(link, 'GET', data)
  }

  module.getLists = async function (url, method = 'GET', data = '') {
    await this.sendAjax(url, method, data).then((response) => {
      $('#table-data').html(response.html)
    })
  }

  return module
})(window.jQuery, document, window)
