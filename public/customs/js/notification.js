'use strict'
export const notification = (function () {
  const module = {}
  const Swal = window.Swal

  module.confirm = function (message = "Bạn sẽ không thể hoàn nguyên điều này!") {
    return Swal.fire({
      title: 'Bạn có chắc không?',
      text: message,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý'
    })
  }

  module.success = function (type, message) {
    Swal.fire({
      icon: 'success',
      type,
      title: 'Success!',
      text: message
    })
  }

  module.error = function (message) {
    Swal.fire({
      icon: 'error',
      type: 'error',
      title: 'Error!',
      text: message
    })
  }

  return module
})(window.jQuery, document, window)
