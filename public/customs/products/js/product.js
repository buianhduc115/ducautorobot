'use strict'
import { main } from '../../js/main.js'
import { image } from '../../js/image.js'
import { notification } from '../../js/notification.js'
import { LIST_CATEGORY } from './constant.js'

export const product = (function () {
    const module = {}

    module.getList = async function (element) {
        const url = element.attr('data-url')
        const data = new FormData($('.search-form')[0])
        await main.sendAjax(url, 'POST', data).then((response) => {
            this.renderTable(response.data)
        }).catch((error) => {
            console.log(error)
        })
    }

    module.renderTable = function (data) {
        let html = '';
        let index = 1;
        data.products.map((product) => {
            let editButton = ''
            let deleteButton = ''
            let showButton = ''

            if (product.show.isShow) {
                showButton = `<button class="btn btn-info btn-show" data-bs-toggle="modal"
                data-bs-target="#exampleModal" data-url="${product.show.link}">Show</button>`
            }

            if (product.edit.isShow) {
                editButton = `<button class="btn btn-warning btn-edit" data-bs-toggle="modal"
                data-bs-target="#exampleModal" data-url-update="${product.update.link}" data-url="${product.edit.link}" data-action-name="update">Edit</button>`
            }

            if (product.delete.isShow) {
                deleteButton = `<button class="btn btn-danger btn-delete" data-url='${product.delete.link}'>Delete</button>`
            }
            return `
        <tr>
            <td scope="row">${index++}</td>
            <td scope="row">${product.name}</td>
            <td scope="row">${product.price}</td>
            <td scope="row">${product.categories.names}</td>
            <td scope="row"><img src="${product.image}" width="100"></td>
            <td scope="row">${showButton}${editButton}${deleteButton}</td>
        </tr>

    `
        }).forEach((generatedHtml) => {
            html += generatedHtml
        })

        $('.table-body').html(html)
        this.fillPaginateToTable(data.links)
    }

    module.fillPaginateToTable = function (links) {
        const html = `<ul class="pagination pagination-js">
        <li class="page-item ${links.previousPageUrl ? '' : ' disabled'}">
            <a class="page-link" href="${links.previousPageUrl}" data-url="${links.previousPageUrl}"><</a>
        </li>
        ${generatePageLinks(links)}
        <li class="page-item ${links.nextPageUrl ? '' : ' disabled'}">
            <a class="page-link nextPage" href="${links.nextPageUrl}" data-url="${links.nextPageUrl}">></a>
        </li>
        </ul>`

        $('.pagination').html(html)
    }

    function generatePageLinks (links) {
        const currentPage = links.currentPage

        let pageLinks = ''

        for (let page = 1; page <= links.totalPage; page++) {
            const isActive = (page === currentPage) ? ' active' : ''
            pageLinks += `<li class="page-item${isActive}">
            <a class="page-link" href="#" data-url="${getPageUrl(page, links.pathName)}">${page}</a>
            </li>`
        }
        return pageLinks
    }

    function getPageUrl (page, pathName) {
        return `${pathName}?page=${page}`
    }

    module.save = function (element) {
        const type = element.attr('data-action-name')
        const url = element.attr('data-url')
        const data = new FormData(element[0])

        if (type === 'update') {
            data.append('_method', 'PUT')
        }
        main.sendAjax(url, 'POST', data).then(function (response) {
            image.remove()
            $('#exampleModal').hide()
            element[0].reset()
            $('.modal-backdrop').remove()
            notification.success('Saved!', 'Dữ liệu đã được lưu thành công.')
        }).catch(function (error) {
            console.log(error.responseJSON)
            main.renderError(error.responseJSON.errors)
            notification.error('Dữ liệu đã lưu không thành công.')
        })
    }

    module.selectCategory = async function (ids = null) {
        const url = LIST_CATEGORY
        const data = await main.sendAjax(url, 'GET')
        const select = $('.select-categories')
        let html = "<option value=''>Tìm Danh Mục</option>"

        $.each(data.data, function (key, category) {
            html += `<option value="${category.id}" ${ids != null && ids.includes(category.id) ? 'selected' : ''}>${category.name}</option>`
        })
        select.html(html)
    }

    module.fillDataForm = async function (element) {
        const url = $(element).attr('data-url')
        const data = await main.sendAjax(url, 'GET')
        this.selectCategory(data.data.categories.ids)
        $('.name-product').attr('value', data.data.name)
        $('.price-product').attr('value', data.data.price)
        $('.stock-product').attr('value', data.data.stock)
        $('.o-image').attr('src', data.data.image)
        $('.descriptions-product').val(data.data.descriptions)
    }

    module.delete = function (element) {
        const url = element.attr('data-url')
        notification.confirm().then((result) => {
            if (result.isConfirmed) {
                main.sendAjax(url, 'DELETE').then(function (response) {
                    notification.success('Deleted!', 'Dữ liệu đã được xóa thành công.')
                    product.getList($('.search-form'))
                }).catch(function () {
                    notification.error('Dữ liệu đã bị xóa không thành công.')
                })
            }
        })
    }

    return module
})(window.jQuery, window, document)
